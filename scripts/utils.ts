import child_process from "child_process";
import fs from "fs";
import path from "path";

export const PROJECT_ROOT: string = path.normalize(path.dirname(__dirname));
export const BINARY_ROOT: string = (function () {
  switch (process.platform) {
    case "win32":
    case "cygwin":
      return path.join(PROJECT_ROOT, "bin", "windows");
    case "darwin":
      return path.join(PROJECT_ROOT, "bin", "mac");
    default:
      return path.join(PROJECT_ROOT, "bin", "linux");
  }
})();

export function getExecutablePath(...src: string[]): string {
  const p = path.join(BINARY_ROOT, ...src);
  switch (process.platform) {
    case "win32":
    case "cygwin":
      return p + ".exe";
    default:
      return p;
  }
}

export function waitForSuccessfulExit(child: child_process.ChildProcess): Promise<void> {
  return new Promise((resolve, reject) => {
    child.on("exit", code => {
      if (code === 0) {
        resolve();
      } else {
        reject(new Error("process exited with error code " + code));
      }
    });
    child.on("error", reject);
  });
}

export async function cleanTempFile(tmpFilePath: string): Promise<string> {
  await fs.promises.mkdir(path.dirname(tmpFilePath), { recursive: true });
  try {
    await fs.promises.unlink(tmpFilePath);
  } catch (e) {
    if ((e as any).code !== "ENOENT") {
      throw e;
    }
  }
  return tmpFilePath;
}

export async function fileExists(filePath: string): Promise<boolean> {
  return new Promise((resolve, _) => {
    fs.access(filePath, err => resolve(err === null));
  });
}

export async function recreateDir(dirPath: string): Promise<void> {
  try {
    await fs.promises.rmdir(dirPath, { recursive: true });
  } catch (err) {
    // Ignore missing directory errors
    if ((err as any).code !== "ENOENT") {
      throw err;
    }
  }
  await fs.promises.mkdir(dirPath, { recursive: true });
}

export async function createFile(filePath: string, fileContents: any): Promise<void> {
  await fs.promises.mkdir(path.dirname(filePath), { recursive: true });
  return fs.promises.writeFile(filePath, fileContents);
}

export async function createTextFile(filePath: string, fileContents: string): Promise<void> {
  await fs.promises.mkdir(path.dirname(filePath), { recursive: true });
  return fs.promises.writeFile(filePath, fileContents, { encoding: "utf8" });
}

export const readFile: (filePath: string) => Promise<Buffer> = fs.promises.readFile;

export async function readTextFile(filePath: string): Promise<string> {
  return fs.promises.readFile(filePath, { encoding: "utf8" });
}

export async function copyFile(srcPath: string, destPath: string): Promise<void> {
  await fs.promises.mkdir(path.dirname(destPath), { recursive: true });
  return fs.promises.copyFile(srcPath, destPath);
}

export const removeFile: (filePath: string) => Promise<void> = fs.promises.unlink;

export const renameFile: (srcPath: string, destPath: string) => Promise<void> = fs.promises.rename;

export async function* walkTree(dirPath: string, kind?: "files" | "dirs"): AsyncIterable<string> {
  const yieldFiles = kind === undefined || kind === "files";
  const yieldDirs = kind === undefined || kind === "dirs";
  for (const dirent of await fs.promises.readdir(dirPath, { withFileTypes: true })) {
    if (dirent.isDirectory()) {
      if (yieldDirs) {
        yield dirent.name;
      }
      for await (const child of walkTree(path.join(dirPath, dirent.name), kind)) {
        yield path.join(dirent.name, child);
      }
    } else if (yieldFiles) {
      yield dirent.name;
    }
  }
}
