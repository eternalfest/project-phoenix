import path from "path";

import { mapMovie as mapMovieStrings } from "@eternalfest/obf/map-strings";
import { asMap } from "@eternalfest/obf/obf";
import { emitSwf } from "swf-emitter";
import { parseSwf } from "swf-parser";
import { CompressionMethod, Movie } from "swf-types";
import { createFile, PROJECT_ROOT, readFile, readTextFile } from "../utils";
import { protectStrings } from "./protect-strings";

function invertMap<K, V>(map: Map<K, V>): Map<V, K> {
  const inverted: Map<V, K> = new Map();
  for (const [k, v] of map) {
    if (inverted.has(v)) {
      throw new Error(`Duplicate value: ${v}`);
    }
    inverted.set(v, k);
  }
  return inverted;
}

export async function deobfuscate(swfPath: string, outPath: string): Promise<void> {
  const mappingsPath = path.join(PROJECT_ROOT, "game-types", "src", "lib", "hf.map.json");
  const obfuMappings = asMap(JSON.parse(await readTextFile(mappingsPath)));
  const deobfuMappings = invertMap(obfuMappings);

  const movie: Movie = parseSwf(await readFile(swfPath));
  const protectedPrefix = "@<protected>@";

  // Protect small strings from deobfuscation.
  // Don't protected larger strings, as they may be false positives (sprite names, for example).
  protectStrings(movie, s => {
    if (s.length <= 2 && deobfuMappings.has(s)) {
      return protectedPrefix + s;
    }
    return null;
  });

  mapMovieStrings(s => {
    if (s.startsWith(protectedPrefix)) {
      return s.substr(protectedPrefix.length);
    }

    const clear = deobfuMappings.get(s);
    return clear === undefined ? s : clear;
  }, movie);

  await createFile(outPath, emitSwf(movie, CompressionMethod.Deflate));
}
