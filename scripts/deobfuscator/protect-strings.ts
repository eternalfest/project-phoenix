import { ActionType } from "avm1-types/action-type";
import { Action } from "avm1-types/cfg/action";
import { DefineFunction, DefineFunction2 } from "avm1-types/cfg/actions";
import { Cfg } from "avm1-types/cfg/cfg";
import { CfgBlock } from "avm1-types/cfg/cfg-block";
import { CfgFlow } from "avm1-types/cfg/cfg-flow";
import { CfgFlowType } from "avm1-types/cfg/cfg-flow-type";
import { PushValue } from "avm1-types/push-value";
import { PushValueType } from "avm1-types/push-value-type";
import { Movie } from "swf-types";
import { DefineSprite } from "swf-types/tags";
import { CfgVisitor, visitCfgsInTags } from "./visitors";

export function protectStrings(movie: Movie, protect: (s: string) => string | null): void {
  const visitor = new ProtectStringsVisitor(protect);
  visitCfgsInTags(movie.tags, visitor);
}

class ProtectStringsVisitor implements CfgVisitor {

  private protect: (s: string) => string | null;
  private location: string;
  private constantPool: string[] | null;
  private addedConstants: Map<string, number>;
  private allowConstantPoolActions: boolean;

  constructor(protect: (s: string) => string | null, location: string = "$") {
    this.protect = protect;
    this.location = location;
    this.constantPool = null;
    this.addedConstants = new Map();
    this.addedConstants.set("", 0); // TODO remove
    this.allowConstantPoolActions = true;
  }

  visit(cfg: Cfg): Cfg {
    const poolAction = this.removeAndStoreLeadingConstantPool(cfg);

    const values = analyzeCfg(cfg, msg => this.error(msg));

    const originalPoolLength = this.constantPool === null ? 0 : this.constantPool.length;
    for (const [value, metadata] of values) {
      if (metadata.usedAsData) {
        if (metadata.usedAsKey) {
          this.warn(`value ${value} used both as key and data`);
        }

        if (value.type === PushValueType.String) {
          const replaced = (this.protect)(value.value);
          if (replaced !== null) {
            value.value = replaced;
          }
        } else if (value.type === PushValueType.Constant) {
          if (this.constantPool === null || value.value < 0 || value.value >= originalPoolLength) {
            this.error(`invalid constant pool index: ${value.value}`);
          }
          const str = this.constantPool[value.value];
          const replaced = (this.protect)(str);
          if (replaced !== null) {
            let idx = this.addedConstants.get(replaced);
            if (idx === undefined) {
              idx = this.constantPool.length;
              this.constantPool.push(replaced);
              this.addedConstants.set(replaced, idx);
            }
            value.value = idx;
          }
        }
      } else if (!metadata.usedAsKey) {
        this.warn(`value ${value} isn't used anywhere`);
      }
    }

    if (poolAction !== undefined) {
      cfg.blocks[0].actions.unshift(poolAction);
    }

    this.applyFlareWorkarounds(cfg);

    return cfg;
  }

  enterSprite(spriteTag: DefineSprite): CfgVisitor {
    return new ProtectStringsVisitor(this.protect, `${this.location}::${spriteTag.id}`);
  }

  enterFunction(funcAction: DefineFunction | DefineFunction2): CfgVisitor {
    const name = funcAction.name === "" ? "<anon>" : funcAction.name;
    const vis = new ProtectStringsVisitor(this.protect, `${this.location}.${name}`);
    vis.constantPool = this.constantPool;
    vis.allowConstantPoolActions = false;
    return vis;
  }

  enterWithBlock(): never {
    throw new Error("with blocks aren't supported");
  }

  enterTryBlock(): never {
    throw new Error("try blocks aren't supported");
  }

  private error(msg: string): never {
    throw new Error(`at ${this.location}: ${msg}`);
  }

  private warn(msg: string): void {
    console.log(`[WARNING] at ${this.location}: ${msg}`);
  }

  private removeAndStoreLeadingConstantPool(cfg: Cfg): Action | undefined {
    const headActions = cfg.blocks[0].actions;
    if (headActions.length > 0) {
      if (headActions[0].action === ActionType.ConstantPool) {
        if (!this.allowConstantPoolActions) {
          this.error("constant pools aren't supported in nested Cfgs");
        }
        this.constantPool = headActions[0].pool;
        return headActions.shift();
      }
    }
    return undefined;
  }

  private applyFlareWorkarounds(cfg: Cfg) {
    // Flare doesn't decompile correctly when multiple blocks jump at the end of the CFG,
    // so we help it by adding an noop block at the end of the function.
    const endLabel = "final-empty";
    const endBlock: CfgBlock = {
      label: endLabel,
      actions: [{ action: ActionType.Push, values: [{ type: PushValueType.Undefined }]}],
      flow: {
        type: CfgFlowType.Simple,
        next: null,
      },
    };

    for (const block of cfg.blocks) {
      switch (block.flow.type) {
        case CfgFlowType.Simple:
          if (block.flow.next === null) {
            block.flow.next = endLabel;
          }
          break;
        case CfgFlowType.If:
          if (block.flow.trueTarget === null) {
            block.flow.trueTarget = endLabel;
          }
          if (block.flow.falseTarget === null) {
            block.flow.falseTarget = endLabel;
          }
          break;
        default:
          break;
      }
    }

    cfg.blocks.push(endBlock);
  }

}

interface ValueMetadata {
  usedAsKey: boolean;
  usedAsData: boolean;
}

function mergeMetadata(target: ValueMetadata, source: ValueMetadata): void {
  target.usedAsKey = target.usedAsKey || source.usedAsKey;
  target.usedAsData = target.usedAsData || source.usedAsData;
}

interface UnknownValue {
  readonly type: null;
  readonly stackRef: number | null;
}

interface BlockSimResult {
  readonly values: ReadonlyMap<PushValue, ValueMetadata>;
  readonly popped: ReadonlyArray<ValueMetadata>;
  readonly pushed: ReadonlyArray<PushValue | UnknownValue>;
}

function analyzeCfg(cfg: Cfg, onError: (msg: string) => never): Map<PushValue, ValueMetadata> {
  type ValueStack = Set<PushValue>[];
  interface BlockMeta extends BlockSimResult {
    readonly block: CfgBlock;
    readonly outBlocks: Set<BlockMeta>;
    stack?: ValueStack;
  }

  function stackEquals(s1: ValueStack | undefined, s2: ValueStack | undefined): boolean {
    if (s1 === s2) {
      return true;
    } else if (s1 === undefined || s2 === undefined) {
      return false;
    } else if (s1.length !== s2.length) {
      return false;
    } else {
      for (let i = 0; i < s1.length; i++) {
        const v1 = s1[i];
        const v2 = s2[i];
        if (v1.size !== v2.size) {
          return false;
        }
        for (const v of v1) {
          if (!v2.has(v)) {
            return false;
          }
        }
      }
      return true;
    }
  }

  const blockMap: Map<string, BlockMeta> = new Map(cfg.blocks.map(block => [block.label, {
    block,
    outBlocks: new Set(),
    ...simulateActions(block, msg => onError(`in block ${block.label}: ${msg}`)),
  }]));

  // Initialize edges
  for (const block of blockMap.values()) {
    const edges = getOutgoingEdges(block.block.flow);
    if (edges === null) {
      onError(`unsupported block type ${CfgFlowType[block.block.flow.type]} for ${block.block.label}`);
    }

    for (const edge of edges) {
      if (edge === null) {
        // ignore edges exiting the cfg
        continue;
      }

      const next = blockMap.get(edge);
      if (next === undefined) {
        onError(`unknown block label '${edge}'`);
      }

      block.outBlocks.add(next);
    }
  }

  // Determine iteratively the stack content at the start of each block
  const workSet: Set<BlockMeta> = new Set([blockMap.get(cfg.blocks[0].label)!]);
  for (const block of workSet) {
    workSet.delete(block);

    // Apply the stack changes
    if (block.stack === undefined) {
      block.stack = [];
    }
    const stack = [...block.stack];

    if (stack.length < block.popped.length) {
      onError(`in block ${block.block.label}: not enough items on stack `
        + `(expected ${block.popped.length}, got ${stack.length})`);
    }
    const popped = stack.splice(stack.length - block.popped.length, block.popped.length);
    for (const pushed of block.pushed) {
      if (pushed.type === null) {
        stack.push(new Set(pushed.stackRef !== null ? popped[pushed.stackRef] : []));
      } else {
        stack.push(new Set([pushed]));
      }
    }

    // Propagate stack changes to the next blocks
    for (const next of block.outBlocks) {
      if (next.stack !== undefined) {
        if (next.stack.length !== stack.length) {
          onError(`on block edge ${block.block.label} -> ${next.block.label}: stack sizes don't match `
            + `(expected ${stack.length}, got ${next.stack.length})`);
        }

        // Merge stack sets
        for (let i = 0; i < stack.length; i++) {
          const s = stack[i];
          for (const v of next.stack[i]) {
            s.add(v);
          }
        }
      }

      if (!stackEquals(stack, next.stack)) {
        next.stack = stack;
        // the stack for the next node changed; we need to process it again
        workSet.add(next);
      }
    }
  }

  // Collect value metadata
  const allValues: Map<PushValue, ValueMetadata> = new Map((function* () {
    for (const block of blockMap.values()) {
      yield* block.values;
    }
  })());

  for (const block of blockMap.values()) {
    if (block.stack === undefined) {
      onError(`block ${block.block.label} is unreachable`);
    }

    let i = block.stack.length - block.popped.length;
    for (const poppedMeta of block.popped) {
      for (const value of block.stack[i++]) {
        const valueMeta = allValues.get(value);
        if (valueMeta === undefined) {
          onError(`missing metadata for value: ${JSON.stringify(value)}`);
        }
        mergeMetadata(valueMeta, poppedMeta);
      }
    }
  }

  return allValues;
}

function getOutgoingEdges(flow: CfgFlow): (string | null)[] | null {
  switch (flow.type) {
    case CfgFlowType.If:
      return [flow.trueTarget, flow.falseTarget];
    case CfgFlowType.Return:
      return [];
    case CfgFlowType.Simple:
      return [flow.next];
    case CfgFlowType.Throw:
      return [];
    case CfgFlowType.WaitForFrame:
    case CfgFlowType.WaitForFrame2:
      return [flow.loadingTarget, flow.readyTarget];
    default:
      return null;
  }
}

function simulateActions(block: CfgBlock, onError: (msg: string) => never, logDebug: boolean = false): BlockSimResult {
  interface SimValue extends ValueMetadata {
    value: PushValue | UnknownValue;
  }

  const stack: SimValue[] = [];
  const underflows: SimValue[] = [];
  const values: Map<PushValue, ValueMetadata> = new Map();

  function debug(msg: () => string): void {
    if (logDebug) {
      console.log(msg());
    }
  }

  function pop(): SimValue {
    const value = stack.pop();
    if (value === undefined) {
      const unk = {
        value: { type: null, stackRef: underflows.length },
        usedAsKey: false,
        usedAsData: false,
      };

      underflows.push(unk);
      debug(() => `  [${stack.length}] popped empty stack: underflow #${unk.value.stackRef}`);
      return unk;
    }
    debug(() => `  [${stack.length}] popped value: ${JSON.stringify(value.value)}`);
    return value;
  }

  function push(v: SimValue): SimValue {
    if (v.value.type !== null) {
      values.set(v.value, v);
    }
    debug(() => `  [${stack.length}] pushed value: ${JSON.stringify(v.value)}`);
    stack.push(v);
    return v;
  }

  function asPositiveInt(value: SimValue): number {
    value.usedAsData = true;
    const v = value.value;
    if (v === undefined || v.type === null) {
      onError("tried to convert unknown value into a int");
    }
    if (v.type === PushValueType.Sint32 || v.type === PushValueType.Float32 || v.type === PushValueType.Float64) {
      const n = v.value;
      if (n < 0 || Math.floor(n) !== n) {
        onError("tried to convert a float or a negative number into a int");
      }
      return n;
    }
    onError(`tried to convert a value of type ${PushValueType[v.type]} into an int`);
  }

  function popAs(kind: PopKind): void {
    switch (kind) {
      case "key":
        pop().usedAsKey = true;
        break;
      case "data":
        pop().usedAsData = true;
        break;
      case "varia":
        let count = asPositiveInt(pop());
        while (count-- > 0) {
          pop().usedAsData = true;
        }
        break;
      case "pairs":
        let props = asPositiveInt(pop());
        while (props-- > 0) {
          pop().usedAsData = true;
          pop().usedAsKey = true;
        }
        break;
      case null:
        pop();
        break;
      default:
        throw new Error("InvalidPopKind");
    }
  }

  for (const action of block.actions) {
    debug(() => "Action " + ActionType[action.action]);
    switch (action.action) {
      case ActionType.Push:
        for (const value of action.values) {
          push({ value, usedAsData: false, usedAsKey: false });
        }
        break;

      case ActionType.PushDuplicate:
        const dup = pop();
        push(dup);
        push(dup);
        break;

      case ActionType.StoreRegister:
        const reg = pop();
        reg.usedAsData = true;
        push(reg);
        break;

      default:
        const info = ACTION_TABLE.get(action.action);
        if (info === undefined) {
          onError("Unsupported action type " + ActionType[action.action]);
        }

        for (let i = 1; i < info.length; i++) {
          popAs(info[i] as PopKind);
        }

        if (info[0]) {
          push({
            value: { type: null, stackRef: null },
            usedAsData: false,
            usedAsKey: false,
          });
        }
        break;
    }
  }

  const flow = block.flow;
  switch (flow.type) {
    case CfgFlowType.If:
      debug(() => `End: if ? ${flow.trueTarget} else ${flow.falseTarget}`);
      pop().usedAsData = true;
      break;
    case CfgFlowType.Return:
      debug(() => "End: return");
      pop().usedAsData = true;
      break;
    case CfgFlowType.Simple:
      debug(() => `End: next ${flow.next}`);
      break;
    case CfgFlowType.Throw:
      debug(() => "End: throw");
      pop().usedAsData = true;
      break;
    case CfgFlowType.WaitForFrame:
      debug(() => `End: waitForFrame #${flow.frame} ? ${flow.readyTarget} else ${flow.loadingTarget}`);
      break;
    case CfgFlowType.WaitForFrame2:
      debug(() => `End: waitForFrame2; loaded ? ${flow.readyTarget} else ${flow.loadingTarget}`);
      pop().usedAsKey = true;
      break;
    case CfgFlowType.Error:
      onError(`invalid block: ${flow.error}`);
      // tslint:disable-next-line no-switch-case-fall-through
    case CfgFlowType.With:
    case CfgFlowType.Try:
    // tslint:disable-next-line switch-final-break
    default:
      onError(`unsupported block flow: ${CfgFlowType[flow.type]}`);
  }

  debug(() => "Underflows: " + JSON.stringify(underflows));
  debug(() => "Stack result: " + JSON.stringify(stack));
  return { values, popped: underflows, pushed: stack.map(v => v.value) };
}

type PopKind = "data" | "key" | "varia" | "pairs" | null;
const ACTION_TABLE: Map<ActionType, [boolean, ...PopKind[]]> = new Map([
  [ActionType.Add2            , [true , "data"  , "data"  ]],
  [ActionType.And             , [true , "data"  , "data"  ]],
  [ActionType.BitAnd          , [true , "data"  , "data"  ]],
  [ActionType.BitLShift       , [true , "data"  , "data"  ]],
  [ActionType.BitOr           , [true , "data"  , "data"  ]],
  [ActionType.BitRShift       , [true , "data"  , "data"  ]],
  [ActionType.BitURShift      , [true , "data"  , "data"  ]],
  [ActionType.BitXor          , [true , "data"  , "data"  ]],
  [ActionType.CallFunction    , [true , "key"   , "varia" ]],
  [ActionType.CallMethod      , [true , "key"   , "data"  , "varia" ]],
  [ActionType.CloneSprite     , [false, "data"  , "key"   , "key"   ]],
  [ActionType.Decrement       , [true , "data"  ]],
  [ActionType.DefineFunction2 , [true]],
  [ActionType.DefineLocal2    , [false, "key"   ]],
  [ActionType.DefineLocal     , [false, "data"  , "key"   ]],
  [ActionType.Divide          , [true , "data"  , "data"  ]],
  [ActionType.Equals2         , [true , "data"  , "data"  ]],
  [ActionType.Extends         , [false, "data"  , "data"  ]],
  [ActionType.GetMember       , [true , "key"   , "data"  ]],
  [ActionType.GetProperty     , [true , "data"  , "data"  ]],
  [ActionType.GetVariable     , [true , "key"   ]],
  [ActionType.GotoFrame2      , [false, "key"   ]],
  [ActionType.GotoFrame       , [false]],
  [ActionType.GotoLabel       , [false]],
  [ActionType.Greater         , [true , "data"  , "data"  ]],
  [ActionType.Increment       , [true , "data"  ]],
  [ActionType.InitArray       , [true , "varia" ]],
  [ActionType.InitObject      , [true , "pairs" ]],
  [ActionType.Less2           , [true , "data"  , "data"  ]],
  [ActionType.Modulo          , [true , "data"  , "data"  ]],
  [ActionType.Multiply        , [true , "data"  , "data"  ]],
  [ActionType.NewMethod       , [true , "key"   , "data"  , "varia" ]],
  [ActionType.NewObject       , [true , "key"   , "varia" ]],
  [ActionType.Not             , [true , "data"  ]],
  [ActionType.Play            , [false]],
  [ActionType.Pop             , [false, null    ]],
  [ActionType.RandomNumber    , [true , "data"  ]],
  [ActionType.RemoveSprite    , [false, "key"   ]],
  [ActionType.SetMember       , [false, "data"  , "key"   , "data"  ]],
  [ActionType.SetProperty     , [true , "data"  , "data"  , "data"  ]],
  [ActionType.SetVariable     , [false, "data"  , "key"   ]],
  [ActionType.Stop            , [false]],
  [ActionType.StrictEquals    , [true , "data"  , "data"  ]],
  [ActionType.Subtract        , [true , "data"  , "data"  ]],
  [ActionType.ToInteger       , [true , "data"  ]],
  [ActionType.ToString        , [true , "data"  ]],
]);
