
import { cfgToBytes } from "avm1-emitter";
import { parseCfg } from "avm1-parser";
import { ActionType } from "avm1-types/action-type";
import { DefineFunction, DefineFunction2 } from "avm1-types/cfg/actions";
import { Cfg } from "avm1-types/cfg/cfg";
import { CfgBlock } from "avm1-types/cfg/cfg-block";
import { CfgFlowType } from "avm1-types/cfg/cfg-flow-type";
import { Tag, TagType } from "swf-types";
import { DefineSprite } from "swf-types/tags";

export interface CfgTryBlockVisitors {
  try: CfgVisitor | null;
  catch: CfgVisitor | null;
  finally: CfgVisitor | null;
}

export interface CfgVisitor {
  visit(cfg: Cfg): Cfg | null;
  enterSprite(spriteTag: DefineSprite): CfgVisitor | null;
  enterFunction(funcAction: DefineFunction | DefineFunction2): CfgVisitor | null;
  // TODO: use more specific type
  enterWithBlock(cfgBlock: CfgBlock): CfgVisitor | null;
  // TODO: use more specific type
  enterTryBlock(cfgBlock: CfgBlock): CfgTryBlockVisitors | null;
}

export function visitCfgsInTags(tags: ReadonlyArray<Tag>, visitor: CfgVisitor): void {
  for (const tag of tags) {
    switch (tag.type) {
      case TagType.DefineSprite:
        const nested = visitor.enterSprite(tag);
        if (nested !== null) {
          visitCfgsInTags(tag.tags, visitor);
        }
        break;
      case TagType.DoAction:
      case TagType.DoInitAction:
        const cfg = parseCfg(tag.actions);
        const newCfg = visitCfg(cfg, visitor);
        if (newCfg !== null) {
          // TODO remove; needed because `DoAction.action` is readonly
          (tag as any).actions = cfgToBytes(newCfg);
        }
        break;
      default:
        break;
    }
  }
}

function visitNestedCfg(nested: Cfg, visitor: CfgVisitor | null): Cfg {
  if (visitor !== null) {
    const newCfg = visitCfg(nested, visitor);
    if (newCfg !== null) {
      nested = newCfg;
    }
  }
  return nested;
}

export function visitCfg(cfg: Cfg, visitor: CfgVisitor): Cfg | null {
  const newCfg = visitor.visit(cfg);
  if (newCfg !== null) {
    cfg = newCfg;
  }

  for (const block of cfg.blocks) {
    for (const action of block.actions) {
      if (action.action !== ActionType.DefineFunction && action.action !== ActionType.DefineFunction2) {
        continue;
      }

      action.body = visitNestedCfg(action.body, visitor.enterFunction(action));
    }

    if (block.flow.type === CfgFlowType.With) {
      block.flow.body = visitNestedCfg(block.flow.body, visitor.enterWithBlock(block));

    } else if (block.flow.type === CfgFlowType.Try) {
      const visitors = visitor.enterTryBlock(block);
      if (visitors !== null) {
        block.flow.try = visitNestedCfg(block.flow.try, visitors.try);
        if (block.flow.catch !== undefined) {
          block.flow.catch.body = visitNestedCfg(block.flow.catch.body, visitors.catch);
        }
        if (block.flow.finally !== undefined) {
          block.flow.finally = visitNestedCfg(block.flow.finally, visitors.finally);
        }
      }
    }
  }

  return newCfg;
}
