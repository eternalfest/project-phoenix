
import { getAs2Map, mergeMaps, obfuscateBytes } from "@eternalfest/obf/obf";
import path from "path";
import { createFile, PROJECT_ROOT, readFile, readTextFile } from "./utils";

async function makeObfuscationMap(gameMappingsPath: string, extraPath: string): Promise<Map<string, string>> {
  const gameMap: Record<string, string> = JSON.parse(
    await readTextFile(gameMappingsPath),
  );
  const as2Map = await getAs2Map();
  const extra: string[] = JSON.parse(await readTextFile(extraPath));
  return mergeMaps(gameMap, mergeMaps(as2Map, extra));
}

export async function obfuscate(swfPath: string, outPath: string): Promise<void> {
  const movieBytes = await readFile(swfPath);
  const obfuMap = await makeObfuscationMap(
    path.join(PROJECT_ROOT, "game-types", "src", "lib", "hf.map.json"),
    path.join(PROJECT_ROOT, "extra-as2.map.json"),
  );

  const result = obfuscateBytes(movieBytes, "loader", obfuMap);
  await createFile(outPath, result.bytes);
}
