import child_process from "child_process";
import path from "path";
import { getExecutablePath, renameFile, waitForSuccessfulExit } from "./utils";

function getFlareOutPath(swfPath: string): string {
  const ext = path.extname(swfPath);
  return swfPath.substring(0, swfPath.length - ext.length) + ".flr";
}

export async function decompile(swfPath: string, outPath: string): Promise<void> {
  const flarePath = getExecutablePath("flare/flare");
  const flareArgs = [swfPath];
  const childProc = child_process.spawn(flarePath, flareArgs, { stdio: "pipe" });

  childProc.stderr.setEncoding("utf8");
  childProc.stderr.on("data", (chunk: string) => {
    for (const line of chunk.split(/\r\n|\n/)) {
      if (line.startsWith("Unresolved label:")) {
        continue; // Ignore these errors
      } else if (line.length > 0) {
        console.log(line);
      }
    }
  });

  await waitForSuccessfulExit(childProc);

  const flareOutFile = getFlareOutPath(swfPath);
  if (path.normalize(flareOutFile) !== path.normalize(outPath)) {
    await renameFile(flareOutFile, outPath);
  }
}
