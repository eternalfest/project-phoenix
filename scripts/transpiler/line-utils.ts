
export interface Line {
  readonly pos: number;
  readonly indent: number;
  readonly line: string;
}

export interface LinesIterator extends Iterable<Line> {
  next(): Line | undefined;
  putBack(line: Line): void;
  iterBlockContents(): LinesIterator;
}

export function iterateLines(lines: Iterable<string>): LinesIterator {
  if (typeof lines === "string" || lines instanceof String) {
    lines = lines.split("\n");
  }
  return new LinesIter(lines);
}

const INDENT_SIZE: number = 2;
const INDENT_CHUNKS: string[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map(n => " ".repeat(INDENT_SIZE * n));

export function emitIndent(chunks: string[], indent: number): void {
  while (indent > 0) {
    const i = Math.min(INDENT_CHUNKS.length, indent);
    chunks.push(INDENT_CHUNKS[i]);
    indent -= i;
  }
}

function makeIterator<T>(iter: { next(): T | undefined }): Iterator<T> {
  return {
    next: () => {
      const line = iter.next();
      return { value: line!, done: line === undefined };
    },
  };
}

class LinesIter implements LinesIterator {
  private pos: number;
  private lines: Iterator<string>;
  private nextLine?: Line;
  private nextNextLine?: Line;

  constructor(lines: Iterable<string>) {
    this.pos = 0;
    this.lines = lines[Symbol.iterator]();
  }

  public next(): Line | undefined {
    if (this.nextLine === undefined) {
      this.retrieveNextLine();
    }
    const ret = this.nextLine;
    this.nextLine = this.nextNextLine;
    this.nextNextLine = undefined;
    return ret;
  }

  public [Symbol.iterator](): Iterator<Line> {
    return makeIterator(this);
  }

  public putBack(line: Line): void {
    if (this.nextNextLine !== undefined) {
      throw new Error("Can't put back line");
    }
    this.nextNextLine = this.nextLine;
    this.nextLine = line;
  }

  public iterBlockContents(): LinesIterator {
    return new BlockContentsIter(this);
  }

  private retrieveNextLine(): void {
    while (true) {
      const item = this.lines.next();
      if (item.done !== false) {
        return;
      }

      this.pos++;
      const lineWithIndent = item.value.trimRight();
      const line = lineWithIndent.trimLeft();

      if (line.length === 0) {
        continue; // skip empty lines
      }

      const whitespace = lineWithIndent.length - line.length;
      if (whitespace % INDENT_SIZE !== 0) {
        throw new Error(`invalid indent size = ${whitespace} at l.${this.pos}`);
      }
      const indent = whitespace / INDENT_SIZE;

      // Hack to never have empty blocks `{}`
      const split = line.length - (
          line.endsWith("else {}") ? 1 :
          line.endsWith(") {};")   ? 2 : 0);
      if (split < line.length) {
        this.nextNextLine = { pos: this.pos, indent, line: line.substring(split) };
      }

      this.nextLine = { pos: this.pos, indent, line: line.substring(0, split) };
      return;
    }
  }
}

class BlockContentsIter implements LinesIterator {
  private baseIndent?: number;
  private inner: LinesIterator;

  constructor(inner: LinesIterator) {
    this.inner = inner;
  }

  public next(): Line | undefined {
    const line = this.inner.next();
    if (line === undefined) {
      return undefined;
    }

    if (this.baseIndent === undefined) {
      if (line.line.startsWith("}")) {
        return undefined;
      }
      this.baseIndent = line.indent;
    }

    if (line.indent < this.baseIndent) {
      if (!line.line.startsWith("}")) {
        throw new Error(`l.${line.pos}: deindent without end of block`);
      }
      return undefined;
    }
    return line;
  }

  public [Symbol.iterator](): Iterator<Line> {
    return makeIterator(this);
  }

  public putBack(line: Line): void {
    this.inner.putBack(line);
  }

  public iterBlockContents(): LinesIterator {
    return new BlockContentsIter(this);
  }
}
