import { As2Class, As2Method } from "./as2-class";
import { iterateLines, Line, LinesIterator } from "./line-utils";

export interface MtascOutput {
  classes: ReadonlyMap<string, As2Class>;
  globals: ReadonlyMap<string, string>;
}

export function transpile(source: string): MtascOutput {
  const classList: As2Class[] = [MOVIE_CLIP_CLASS];
  const globals: Map<string, string> = new Map();
  const staticFields: MtascStatic[] = [];

  for (const item of processMovie(iterateLines(source))) {
    switch (item.kind) {
      case "class":
        classList.push(item.cls);
        break;
      case "global":
        if (globals.has(item.name)) {
          throw new Error(`Duplicate global var name '${item.name}'`);
        }
        globals.set(item.name, item.value);
        break;
      case "static":
        staticFields.push(item);
        break;
      default:
        throw new Error("UnknownItemType: " + (item as any).kind);
    }
  }

  const classes = As2Class.resolveSuperClasses(classList);
  classes.delete(MOVIE_CLIP_CLASS.name);
  for (const cls of classList) {
    removeInheritedFields(cls);
  }

  for (const staticField of staticFields) {
    const cls = classes.get(staticField.cls);
    if (cls === undefined) {
      throw new Error(`Unknown class name ${staticField.cls} for static var`);
    }
    addExtraStaticField(cls, staticField.name, staticField.value);
  }

  return { classes, globals };
}

function removeInheritedFields(cls: As2Class): void {
  let cur = cls.superClass;
  while (cur !== null) {
    for (const superMember of cur.members.values()) {
      const member = cls.members.get(superMember.name);
      // This also remove inherited methods which were wrongly detected as fields
      if (!superMember.isStatic && member !== undefined && !member.isStatic && member.kind === "field") {
        cls.removeMember(member.name);
      }
    }
    cur = cur.superClass;
  }
}

function addExtraStaticField(cls: As2Class, name: string, value: string): void {
  const field = cls.members.get(name);
  if (field === undefined) {
    cls.addMember({ kind: "field", isStatic: true, name, value });
  } else if (field.kind === "field" && field.isStatic) {
    field.value = value;
  } else {
    throw new Error(`Class ${cls.name}: field ${name} isn't a static field`);
  }
}

const MOVIE_CLIP_CLASS = new As2Class("MovieClip", null, [
    "_alpha", "blendMode", "cacheAsBitmap", "_currentframe", "_droptarget", "enabled", "filters",
    "focusEnabled", "_focusrect", "forceSmoothing", "_framesloaded", "_height", "_highquality",
    "hitArea", "_lockroot", "menu", "_name", "opaqueBackground", "_parent", "_quality", "_rotation",
    "scale9Grid", "scrollRect", "_soundbuftime", "tabChildren", "tabEnabled", "tabIndex", "_target",
    "_totalframes", "trackAsMenu", "transform", "_url", "useHandCursor", "_visible", "_width", "_x",
    "_xmouse", "_xscale", "_y", "_ymouse", "_yscale",
    // methods actually; we declare them as fields for `removeInheritedFields` to work correctly
    "gotoAndPlay", "gotoAndStop", "onRelease", "onRollOut", "onRollOver", "play", "stop",
  ].map(name => ({ kind: "field", isStatic: false, name, value: null })));

interface MtascClass {
  kind: "class";
  cls: As2Class;
}

interface MtascGlobal {
  kind: "global";
  name: string;
  value: string;
}

interface MtascStatic {
  kind: "static";
  cls: string;
  name: string;
  value: string;
}

type MtascItem = MtascClass | MtascGlobal | MtascStatic;

function* processMovie(lines: LinesIterator): Iterable<MtascItem> {
  const first = lines.next();
  const second = lines.next();
  if (first === undefined || second === undefined ||
    (!first.line.startsWith("movie '") && !second.line.startsWith("//"))) {
    throw new Error("Invalid movie header");
  }
  lines = lines.iterBlockContents();

  for (const { pos, line } of lines) {
    if (!line.endsWith("{")) {
      throw new Error(`#${pos}: expect block in movie, got: ${line}`);
    } else if (line.startsWith("frame")) {
      yield* processFrame(lines.iterBlockContents());
    } else if (line.startsWith("movieClip")) {
      for (const _ of lines.iterBlockContents()) {
        // TODO: extract sprite code?
      }
    } else {
      throw new Error(`"${pos}: unexpected block type in movie: ${line}`);
    }
  }
}

function* processFrame(lines: LinesIterator): Iterable<MtascItem> {
  for (const line of lines) {
    if (line.line === "undefined;") {
      continue; // Ignore 'undefined's introduced by the deobfuscator to help flare
    } else if (line.line.startsWith("if ")) {
      for (const _ of lines.iterBlockContents()) {
        // Skip the 'if's creating the package objects
      }
    } else {
      const parts = splitWithTail(line.line, " = ", 1);
      if (parts.length !== 2) {
        throw new Error(`#${line.pos}: expected assignement statement in frame, got: ${line.line}`);
      }

      if (line.line.endsWith("{")) {
        yield processClassDecl(line, parts[0], parts[1], lines);
      } else {
        const global = processGlobalVarDecl(line, parts[0], parts[1]);
        if (global !== null) {
          yield global;
        }
      }
    }
  }
}

function processGlobalVarDecl(linfo: Line, name: string, value: string): MtascGlobal | MtascStatic | null {
  if (name.includes("__")) {
    return null; // Ignore, this is a saved method
  } else if (name.includes(".")) {
    // Some static vars are declared before their class
    const dot = name.lastIndexOf(".");
    return {
      kind: "static",
      cls: name.substring(0, dot),
      name: name.substring(dot + 1),
      value: fixObjectLiterals(value.substring(0, value.length - 1)),
    };
  }

  const chunksSeparator = " + ";
  const chunks: string[] = [];
  let start = 0;
  // Read string chunks
  while (true) {
    if (value.charAt(start) !== "'") {
      throw new Error(`#${linfo.pos}: global var '${name}' isn't a string: ${value.substring(start)}`);
    }
    start++;

    // Read a string in single quotes
    while (true) {
      const quote = value.indexOf("'", start);
      if (quote < 0) {
        throw new Error(`#${linfo.pos}: missing quote for global var '${name}': ${value.substring(start)}`);
      }

      let nbEscapes = 0;
      while (value.charAt(quote - nbEscapes - 1) === "\\") {
        nbEscapes++;
      }
      chunks.push(value.substring(start, quote - nbEscapes));
      chunks.push("\\".repeat(Math.floor(nbEscapes / 2)));
      start = quote + 1;

      const escaped = nbEscapes % 2 === 1;
      if (escaped) {
        chunks.push("'");
      } else {
        break;
      }
    }

    if (value.startsWith(chunksSeparator, start)) {
      start += chunksSeparator.length;
      if (value.charAt(start) === "(") {
        start++;
      }
    } else {
      break; // this was the last chunk
    }
  }

  return { kind: "global", name, value: chunks.join("") };
}

function processClassDecl(linfo: Line, name: string, header: string, lines: LinesIterator): MtascClass {
  const constructor = processMethodBody(linfo, name.split(".").pop()!, header, lines);

  const superClassLine = lines.next();
  let superClass: string | null = null;
  if (superClassLine !== undefined) {
    const superClassParts = splitWithTail(superClassLine.line, " extends ", 1);
    if (superClassParts.length === 2) {
      superClass = superClassParts[1].substring(0, superClassParts[1].length - 1);
    } else {
      lines.putBack(superClassLine);
    }
  }

  const cls = new As2Class(name, superClass);
  cls.addMember(constructor);
  const methodsToScan: As2Method[] = [constructor];

  const staticPrefix = name + ".";
  const protoPrefix = "v1.";

  for (const curLine of lines) {
    const { pos, line } = curLine;
    if (line.startsWith("var v1 = ") || line.startsWith("v1 = ")) {
      continue; // Prototype declaration, skip it
    }

    const parts = splitWithTail(line, " = ", 1);
    if (parts.length !== 2) {
      lines.putBack(curLine);
      break;
    }

    const [rawMemberName, memberHeader] = parts;

    if (rawMemberName.startsWith(protoPrefix)) {
      if (!memberHeader.endsWith("{")) {
        throw new Error(`#${pos}: expected function declaration on prototype, got: ${line}`);
      }
      const memberName = rawMemberName.substring(protoPrefix.length);
      const method = processMethodBody(curLine, memberName, memberHeader, lines);
      cls.addMember(method);
      methodsToScan.push(method);

    } else if (rawMemberName.startsWith(staticPrefix)) {
      const memberName = rawMemberName.substring(staticPrefix.length);
      if (memberHeader.endsWith(";")) {
        const fieldValue = fixObjectLiterals(memberHeader.substring(0, memberHeader.length - 1));
        cls.addMember({
          kind: "field",
          isStatic: true,
          name: memberName,
          value: fieldValue,
        });
      } else if (memberHeader.endsWith("{")) {
        const method = processMethodBody(curLine, memberName, memberHeader, lines);
        method.isStatic = true;
        cls.addMember(method);
        methodsToScan.push(method);
      } else {
        throw new Error(`#${pos}: expected block start or statement, got: ${memberHeader}`);
      }
    } else {
      lines.putBack(curLine);
      break;
    }

  }

  for (const method of methodsToScan) {
    recordFieldsInLines(cls, method.body);
  }

  return { kind: "class", cls };
}

function processMethodBody(linfo: Line, name: string, header: string, lines: LinesIterator): As2Method {
  const openParen = header.indexOf("(");
  const closeParen = header.indexOf(")", openParen);
  if (openParen < 0 || closeParen < 0) {
    throw new Error(`#${linfo.pos}: invalid header for method '${name}': ${header}`);
  }

  const args = header.substring(openParen, closeParen + 1);
  const body: Line[] = [];
  for (let { pos, indent, line } of lines.iterBlockContents()) {
    indent -= linfo.indent;
    line = fixObjectLiterals(line);
    line = translateSavedMethods(line);
    body.push({ pos, indent, line });
  }

  resolveLabels(body);
  fixBrokenReturnsAndCasesInSwitch(body);
  while (fixLocalVariableDeclarations(body)) {
    // Run the pass until there is nothing to do
  }

  return { kind: "method", isStatic: false, name, args, body };
}

function recordFieldsInLines(cls: As2Class, lines: Line[] | null): void {
  if (lines === null) {
    return;
  }

  function recordFieldsWithRegex(regex: RegExp, line: string, isStatic: boolean): void {
    for (const match of line.matchAll(regex)) {
      const fieldName = match[1];
      if (!cls.members.has(fieldName)) {
        cls.addMember({ kind: "field", isStatic, name: fieldName, value: null });
      }
    }
  }

  for (const { line } of lines) {
    // member fields
    recordFieldsWithRegex(/\bthis\.([A-Za-z_$][A-Za-z0-9_$]*)/g, line, false);

    // static fields
    recordFieldsWithRegex(new RegExp(`\\b${cls.name}\\.([A-Za-z_$][A-Za-z0-9_$]*)`, "g"), line, true);
  }
}

function fixObjectLiterals(line: string): string {
  return line.replace(
    /'([A-Za-z_$][A-Za-z0-9_$]*)':/g,
    "$1:",
  );
}

function translateSavedMethods(line: string): string {
  const match = line.match(/([A-Za-z_][A-Za-z0-9_]*)__([A-Za-z_$][A-Za-z0-9_$]*)\.call\(this/);
  if (match === null) {
    return line;
  }
  const idx = match.index || 0;
  const len = match[0].length;
  const start = line.substring(0, idx);
  const end = line.substring(idx + len);
  const className = match[1].split("_").join(".");
  const methName =  match[2];
  // We put a `_global` here to avoid clashes between package and field names (`levels`, for example)
  return `${start}_global.${className}.prototype.${methName}.call(this${end}`;
}

const COMPARISON_OP_INVERSES: Map<string, string> = new Map([
  ["==", "!="],
  [">=", "<" ],
  ["<=", ">" ],
  [">" , "<="],
  ["<" , ">="],
]);

function resolveLabels(lines: Line[]): void {
  function findLineWithPrefix(idx: number, str: string): [number, Line] | null {
    while (idx < lines.length) {
      const { pos, indent, line } = lines[idx];
      if (line.startsWith(str)) {
        return [idx, { pos, indent, line: line.substring(str.length, line.length - 1) }];
      }
      idx++;
    }
    return null;
  }

  function replaceForLoop(gotoIdx: number, labelIdx: number): number {
    const { pos: breakPos, line: breakLine } = lines[labelIdx + 1];
    if (!breakLine.startsWith("if (") && !breakLine.endsWith(") break;")) {
      throw new Error(`#${breakPos}: expected break statement, got: ${breakLine}`);
    }
    const breakCond = breakLine.substring(4, breakLine.length - 8);

    const nbVars = labelIdx - gotoIdx - 2;
    const varDecls: string[] = [];
    const incrExprs: string[] = [];
    for (let i = 0; i < nbVars; i++) {
      const { pos: varPos, line: varLine } = lines[gotoIdx - nbVars + i];
      if (!varLine.endsWith(";") || !varLine.includes(" = ")) {
        throw new Error(`#${varPos}: expected variable declaration, got: ${varLine}`);
      } else {
        varDecls.push(varLine.substring(varLine.startsWith("var ") ? 4 : 0, varLine.length - 1));
      }

      const { pos: incrPos, line: incrLine } = lines[gotoIdx + 2 + i];
      if (!incrLine.endsWith(";")) {
        throw new Error(`#${incrPos}: expected increment statement, got: ${incrLine}`);
      }
      incrExprs.push(incrLine.substring(0, incrLine.length - 1));
    }

    let loopExpr: string;
    if (breakCond.startsWith("!(")) {
      loopExpr = breakCond.substring(2, breakCond.length - 1);
    } else {
      const parts = splitWithTail(breakCond, " ", 2);
      const binop = parts.length !== 3 ? undefined : COMPARISON_OP_INVERSES.get(parts[1]);
      if (binop === undefined) {
        throw new Error(`#${breakPos}: unknown break condition type: ${breakCond}`);
      }
      loopExpr = `${parts[0]} ${binop} ${parts[2]}`;
    }

    // Do the replacement
    const { pos, indent } = lines[gotoIdx + 1];
    const start = gotoIdx - nbVars;
    lines[start] = { pos, indent, line: `for (var ${varDecls.join(", ")}; ${loopExpr}; ${incrExprs.join(", ")}) {` };
    lines.splice(start + 1, labelIdx - start + 1);
    return start + 1;
  }

  function replaceDoWhileLoop(gotoIdx: number, labelIdx: number): number {
    const { pos: whilePos, indent: whileIndent, line: whileLine } = lines[gotoIdx + 1];
    if (!whileLine.startsWith("while (")) {
      throw new Error(`#${whilePos}: expected while loop, got: ${whileLine}`);
    }
    if (labelIdx - gotoIdx !== 2) {
      throw new Error(`#${lines[labelIdx].pos}: expected label, got: ${lines[labelIdx].line}`);
    }

    let endWhileIdx = labelIdx;
    while (lines[endWhileIdx].indent !== whileIndent) {
      endWhileIdx++;
    }

    // Do the replacement
    const doWhileCond = whileLine.substring(0, whileLine.length - 2).replace(/stored/g, "");
    lines[gotoIdx + 1] = { pos: whilePos, indent: whileIndent, line: "do {" };
    lines[endWhileIdx] = { pos: whilePos, indent: whileIndent, line: `} ${doWhileCond};` };
    lines.splice(labelIdx, 1);
    lines.splice(gotoIdx, 1);
    return gotoIdx + 1;
  }

  function replaceDoWhileLoopAsFor(gotoIdx: number, labelIdx: number): number {
    const { pos: forPos, indent: forIndent } = lines[gotoIdx + 1];
    const { pos: breakPos, line: breakLine } = lines[gotoIdx + 2];

    if (!breakLine.startsWith("if (!") || !breakLine.endsWith(") break;")) {
      throw new Error(`#${breakPos}: expected negated break statement, got: ${breakLine}`);
    }
    const whileCond = breakLine.substring(5, breakLine.length - 8);

    if (labelIdx - gotoIdx !== 3) {
      throw new Error(`#${lines[labelIdx].pos}: expected label, got: ${lines[labelIdx].line}`);
    }

    let endForIdx = labelIdx;
    while (lines[endForIdx].indent !== forIndent) {
      endForIdx++;
    }

    // Do the replacement
    lines[gotoIdx] = { pos: forPos, indent: forIndent, line: "do {" };
    lines[endForIdx] = { pos: breakPos, indent: forIndent, line: `} while${whileCond.replace(/stored/g, "")};` };
    lines.splice(gotoIdx + 1, labelIdx - gotoIdx);
    return labelIdx;
  }

  let idx = 0;
  while (true) {
    const goto = findLineWithPrefix(idx, "goto ");
    if (goto === null) {
      break;
    }
    const [gotoIdx, { pos: gotoPos, line: gotoName }] = goto;
    const label = findLineWithPrefix(gotoIdx + 1, "label ");
    if (label === null) {
      throw new Error(`#${gotoPos}: couldn't find corresponding label for goto`);
    }
    const [labelIdx, { pos: labelPos, line: labelName }] = label;
    if (gotoName !== labelName) {
      throw new Error(`#${labelPos}: goto and label have different names: '${gotoName}' and '${labelName}'`);
    }

    if (lines[gotoIdx + 1].line === "for (;;) {") {
      if (lines[gotoIdx + 2].line.startsWith("if (")) {
        idx = replaceDoWhileLoopAsFor(gotoIdx, labelIdx);
      } else {
        idx = replaceForLoop(gotoIdx, labelIdx);
      }
    } else {
      idx = replaceDoWhileLoop(gotoIdx, labelIdx);
    }
  }
}

function fixBrokenReturnsAndCasesInSwitch(lines: Line[]): void {
  let idx = 0;
  let delta = 0;
  let inSwitch = false;
  let caseIndent = -1;

  while (idx < lines.length) {
    const { pos, indent, line } = lines[idx];

    if (!inSwitch) {
      if (line.startsWith("switch (")) {
        inSwitch = true;
        caseIndent = -1;
      }
      idx++;
      continue;
    }

    const isCase = line.startsWith("case ") && line.endsWith(":");

    if (caseIndent < 0 && !isCase) {
      lines.splice(idx, 1); // remove duplicated default code at start of switch
      continue;
    }

    if (isCase) {
      if (caseIndent < 0) {
        caseIndent = indent;
      } else if (indent < caseIndent + delta) {
        throw new Error(`#${pos}: found case indented by ${indent} (expected ${caseIndent + delta})`);
      } else if (indent > caseIndent + delta) { // fix broken cases
        if (delta > 0) {
          throw new Error(`#${pos}: found nested broken cases, giving up`);
        }
        delta = indent - caseIndent;
        const { pos: breakPos, line: breakLine } = lines[idx - 1];
        let breakIndent = lines[idx - 1].indent;
        if (breakLine !== "break;") {
          throw new Error(`#${breakPos}: expected break, got: ${breakLine}`);
        }
        lines[idx - 1] = { pos: breakPos, indent: caseIndent + 1, line: breakLine };
        while (breakIndent > caseIndent + 1) {
          breakIndent--;
          lines.splice(idx - 1, 0, { pos: breakPos, indent: breakIndent, line: "}" });
          idx++;
        }
      }
    }

    let newLine = line;
    if (line === "case string:") {
      newLine = "case 'string':";
    } else if (line === "case number:") {
      newLine = "case 'number':";
    } else if (line === "case boolean:") {
      newLine = "case 'boolean':";
    } else if (line.startsWith("case $")) {
      newLine = `case '${line.substring(5, line.length - 1)}':`;
    } else if (delta > 0 && caseIndent + delta === indent && line === "}") {
      // remove extra } (corresponding to those we added earlier)
      lines.splice(idx, delta);
      delta++;
    }

    if (indent < caseIndent) {
      inSwitch = false;
    }

    lines[idx] = { pos, indent: indent - delta, line: newLine };
    idx++;
  }
}

function fixLocalVariableDeclarations(lines: Line[]): boolean {
  function isDeclaration(line: string, match: RegExpMatchArray): boolean {
    const start = match.index;
    if (start === undefined || start < 4) {
      return false;
    } else if (start === 4 && line.startsWith("var ")) {
      return true;
    } else if (line.startsWith("for (")) {
      if (line.substring(start - 4, start) === "var ") {
        return true;
      }
      // heuristic for 'for (var i, j; ..., ...)
      const semicolon = line.indexOf(";");
      if (start < semicolon && line.substring(start - 2, start) === ", ") {
        return true;
      }
    }
    return false;
  }

  const declaredVars: Map<string, number> = new Map();

  const varDeclsToRemove: [number, string][] = [];
  const varDeclsToAdd: string[] = [];
  let prevIndent = 0;
  let idx = 0;

  for (const line of lines) {
    const indent = line.indent + (line.line.endsWith("{") ? 1 : 0);

    if (prevIndent > indent) {
      // exit scope
      for (const [varName, scope] of declaredVars) {
        if (scope > indent) {
          declaredVars.delete(varName);
        }
      }
    }

    for (const varMatch of line.line.matchAll(/\bv[0-9]+\b/g)) {
      const isDecl = isDeclaration(line.line, varMatch);
      const varName = varMatch[0];
      const varScope = declaredVars.get(varName);
      const isDefined = varScope !== undefined && varScope <= indent;

      if (isDecl) {
        if (isDefined) {
          // Duplicate variable definition
          varDeclsToRemove.push([idx, varName]);
        } else {
          // Valid variable definition
          declaredVars.set(varName, indent);
        }
      } else {
        if (isDefined) {
          // Valid variable use
        } else {
          // Unknown variable use
          varDeclsToAdd.push(varName);
          // TODO: insert the variable declaration at a better place
          declaredVars.set(varName, 0);
        }
      }
    }

    prevIndent = indent;
    idx++;
  }

  for (const [idx, varName] of varDeclsToRemove) {
    const { pos, indent, line } = lines[idx];
    lines[idx] = { pos, indent, line: line.replace("var " + varName, varName) };
  }

  for (const varName of varDeclsToAdd) {
    // TODO: insert the variable declaration at a better place
    lines.unshift({ pos: lines[0].pos, indent: lines[0].indent, line: `var ${varName};` });
  }

  return varDeclsToAdd.length > 0 || varDeclsToRemove.length > 0;
}

function splitWithTail(str: string, separator: string, limit: number): string[] {
  const parts = str.split(separator);
  if (parts.length <= limit) {
    return parts;
  }
  const tail = parts.splice(limit, parts.length - limit);
  parts.push(tail.join(separator));
  return parts;
}
