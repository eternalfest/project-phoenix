import path from "path";
import { createTextFile, PROJECT_ROOT, readTextFile, recreateDir, walkTree } from "../utils";
import { emitClass } from "./as2-class";
import { HaxeFile, transpile as haxeTranspile } from "./haxe";
import { transpile as mtascTranspile } from "./mtasc";

function dotPathToFile(rootDir: string, dotPath: string, ext: string): string {
  const parts = dotPath.split(".");
  parts.push(parts.pop() + ext);
  return path.join(rootDir, ...parts);
}

function fileToDotPath(file: string, ext: string): string | null {
  const fileExt = path.extname(file);
  if (fileExt !== ext) {
    return null;
  }
  file = file.substring(0, file.length - fileExt.length);
  return file.split(path.sep).join(".");
}

async function readHaxeFilesIn(directory: string, promisesOut: Promise<HaxeFile>[]) {
  for await (const file of walkTree(directory, "files")) {
    const dotPath = fileToDotPath(file, ".hx");
    if (dotPath !== null) {
      promisesOut.push(
        readTextFile(path.join(directory, file))
          .then(contents => ({ name: dotPath, contents })),
      );
    }
  }
}

export async function transpile(gameSrcPath: string, mtascSrcOut: string, globalVarsOut: string, haxeSrcOut: string) {
  const haxeTypingsPath: string = path.join(PROJECT_ROOT, "game-types", "src", "lib");
  const etwinTypesPath: string = path.join(PROJECT_ROOT, "node_modules", "@etwin-haxe", "core", "src");

  const mtascOutPromise = recreateDir(mtascSrcOut);
  const haxeOutPromise = recreateDir(haxeSrcOut);
  const globalVarsPromise = recreateDir(globalVarsOut);
  const promises: Promise<void>[] = [];

  // Transpile raw FLR to MTASC sources
  const rawSource = await readTextFile(gameSrcPath);
  const mtasc = mtascTranspile(rawSource);
  for (const cls of mtasc.classes.values()) {
    const file = dotPathToFile(mtascSrcOut, cls.name, ".as");
    const text = emitClass([], cls).join("");
    promises.push(mtascOutPromise.then(() => createTextFile(file, text)));
  }

  // Write extract global vars
  for (const [name, value] of mtasc.globals) {
    const file = path.join(globalVarsOut, name + ".txt");
    promises.push(globalVarsPromise.then(() => createTextFile(file, value)));
  }

  // Collect Haxe typings
  const typingPromises: Promise<HaxeFile>[] = [];
  await readHaxeFilesIn(haxeTypingsPath, typingPromises);
  await readHaxeFilesIn(etwinTypesPath, typingPromises);

  // Apply the typings and transpile MTASC classes to Haxe
  const haxe = haxeTranspile("hf", mtasc.classes, await Promise.all(typingPromises));
  for (const haxeFile of haxe) {
    const file = dotPathToFile(haxeSrcOut, haxeFile.name, ".hx");
    promises.push(haxeOutPromise.then(() => createTextFile(file, haxeFile.contents)));
  }

  // Wait for all files to be written
  await Promise.all(promises);
}
