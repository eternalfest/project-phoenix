import { normalize as haxeNormalize } from "@eternalfest/obf/obf";
import { As2Class, As2Field, As2Member, As2Method, emitClassMember } from "./as2-class";
import { Line } from "./line-utils";

export interface HaxeFile {
  name: string;
  contents: string;
}

export function* transpile(
    packRoot: string,
    mtascClasses: ReadonlyMap<string, As2Class>,
    typings: HaxeFile[]): Iterable<HaxeFile> {

  const typingsMap: Map<string, string> = new Map(typings.map(t => [t.name, t.contents]));
  const stdClassName = "Std";
  const stdAlias = "HfStd";

  const pathsToReplace = new ClassPathReplacer();
  for (const name of mtascClasses.keys()) {
    // Don't prefix paths without a module, it isn't needed
    if (name.includes(".")) {
      pathsToReplace.addPath(name, `${packRoot}.${name}`);
    }
  }
  pathsToReplace.addPathPrefix("flash", "etwin.flash");
  pathsToReplace.addPath(stdClassName, stdAlias);

  for (const mtascClass of iterateSuperClassesFirst(mtascClasses)) {
    const haxeName = `${packRoot}.${mtascClass.name}`;
    const typings = typingsMap.get(haxeName);
    typingsMap.delete(haxeName);
    if (typings === undefined) {
      console.log(`[WARNING] MTASC class ${mtascClass.name} has no typings; skipping class`);
      continue;
    }

    pathsToReplace.replacePathsInClass(mtascClass);
    yield {
      name: haxeName,
      contents: processClass(haxeName, mtascClass, typings),
    };
  }

  const classesWithoutTypings: string[] = [];
  for (const [haxeName, typings] of typingsMap) {
    classesWithoutTypings.push(haxeName);
    if (haxeName === `${packRoot}.${stdClassName}`) {
      const regex = new RegExp(stdClassName, "g");
      let commentAdded = false;
      yield {
        name: `${packRoot}.${stdAlias}`,
        contents: typings.split("\n").map(l => {
          l = l.replace(regex, stdAlias);
          if (!commentAdded && l.includes(stdAlias)) {
            commentAdded = true;
            return `// Class renamed from ${stdClassName} to ${stdAlias}\n${l}`;
          } else {
            return l;
          }
        }).join("\n"),
      };
    } else {
      yield { name: haxeName, contents: typings };
    }
  }

  if (classesWithoutTypings.length > 0) {
    console.log(
      `[WARNING] ${classesWithoutTypings.length} Haxe classes with no typings `
      + `(declarations copied): ${classesWithoutTypings.join(", ")}`,
    );
  }
}

function* iterateSuperClassesFirst(mtascClasses: ReadonlyMap<string, As2Class>): Iterable<As2Class> {
  const done: Set<string> = new Set();

  function* iterateSuperClasses(cls: As2Class): Iterable<As2Class> {
    if (done.has(cls.name)) {
      return;
    }
    done.add(cls.name);

    if (cls.superName !== null) {
      const superClass = mtascClasses.get(cls.superName);
      if (superClass !== undefined) {
        yield* iterateSuperClasses(superClass);
      }
    }
    yield cls;
  }

  for (const cls of mtascClasses.values()) {
    yield* iterateSuperClasses(cls);
  }
}

class ClassPathReplacer {
  private readonly regexes: [RegExp, string][];

  constructor() {
    this.regexes = [];
  }

  addPath(path: string, replacement: string): void {
    path = path.replace(/[.*+^$[\]()|]/g, "\\$&");
    this.regexes.push([
      new RegExp(`(?<!\\.)\\b${path}\\b`, "g"),
      replacement.replace(/\$/g, "$$"),
    ]);
  }

  addPathPrefix(prefix: string, replacement: string): void {
    this.addPath(prefix + ".", replacement + ".");
  }

  replacePathsInString(str: string): string {
    for (const [regex, replacement] of this.regexes) {
      str = str.replace(regex, replacement);
    }
    return str;
  }

  replacePathsInClass(cls: As2Class): void {
    for (const member of cls.members.values()) {
      switch (member.kind) {
        case "field":
          if (member.value !== null) {
            member.value = this.replacePathsInString(member.value);
          }
          break;
        case "method":
          member.body = member.body === null ? null : member.body.map(line => ({
            pos: line.pos,
            indent: line.indent,
            line: this.replacePathsInString(line.line),
          }));
          break;
        default:
          throw new Error("UnknownMemberKind: " + (member as any).kind);
      }
    }
  }
}

function processClass(haxeName: string, mtascClass: As2Class, typings: string): string {
  const chunks: string[] = [];

  function stringifyKind(member: As2Member): string {
    return `{ kind: ${member.kind}, static: ${member.isStatic} }`;
  }

  const mtascMembers: Map<string, As2Member> = new Map(mtascClass.members);

  let classDeclSeen = false;
  let classEndSeen = false;
  for (const line of typings.split("\n")) {
    if (!classDeclSeen) {
      if (line.startsWith("extern ")) {
        chunks.push(line.substring(7), "\n"); // remove extern, we have sources
        classDeclSeen = true;
      } else {
        chunks.push(line, "\n");
      }
      continue;
    } else if (classEndSeen) {
      chunks.push(line, "\n");
      continue;
    } else if (line.startsWith("}")) {
      chunks.push(line, "\n");
      classEndSeen = true;
      continue;
    }

    // Get Haxe member
    const haxeMember = readHaxeMember(line);
    if (haxeMember === null) {
      chunks.push(line, "\n");
      continue;
    }

    // Get corresponding MTASC member
    const mtascMember = mtascMembers.get(mapHaxeNameToMtasc(mtascClass.name, haxeMember.name));
    if (mtascMember === undefined) {
      console.log(
        `[WARNING] Haxe member ${haxeMember.name} in class ${mtascClass.name} as no source; copying declaration`);
      chunks.push(line, "\n");
      continue;
    }
    mtascMembers.delete(mtascMember.name);

    if (mtascMember.kind !== haxeMember.kind || mtascMember.isStatic !== haxeMember.isStatic) {
      console.log(
        `[WARNING] Incompatible kinds for member ${haxeMember.name} in class ${mtascClass.name}: `
        + `${stringifyKind(haxeMember)} != ${stringifyKind(mtascMember)}; skipping`);
      continue;
    }

    // Inject the member's value
    switch (haxeMember.kind) {
      case "field":
        haxeMember.value = (mtascMember as As2Field).value;
        if (haxeMember.value !== null) {
          haxeMember.value = transpileLine(haxeMember.value);
        }
        break;
      case "method":
        haxeMember.body = (mtascMember as As2Method).body;
        if (haxeMember.body === null) {
          console.log(`[WARNING] Missing method body for member ${haxeMember.name}`);
        } else {
          let body: Iterable<Line> = haxeMember.body;
          for (const pass of TRANSPILE_PASSES) {
            body = pass(body);
          }
          haxeMember.body = [...body];
        }
        break;
      default:
        throw new Error("UnknownMemberKind: " + (haxeMember as any).kind);
    }

    emitClassMember(chunks, 1, haxeMember);
  }

  for (const orphan of mtascMembers.values()) {
    console.log(`[WARNING] Orphan MTASC member ${orphan.name} `
      + `(${stringifyKind(orphan)}) in class ${mtascClass.name}; skipping`);
  }

  chunks.pop(); // remove the last `\n`
  return chunks.join("");
}

function mapHaxeNameToMtasc(className: string, memberName: string): string {
  if (memberName === "new") { // constructors
    return className.split(".").pop()!;
  }
  return haxeNormalize(memberName);
}

function readHaxeMember(line: string): As2Member | null {
  line = line.trim();

  let isPrivate = false;
  let isStatic = false;
  let isDynamic = false;
  let isOverride = false;
  let kind = "";

  while (true) {
    const space = line.indexOf(" ");
    if (space < 0) {
      break;
    }
    const modifier = line.substring(0, space);
    // tslint:disable prefer-switch
    if (modifier === "private") {
      isPrivate = true;
    } else if (modifier === "public") {
      isPrivate = false;
    } else if (modifier === "static") {
      isStatic = true;
    } else if (modifier === "dynamic") {
      isDynamic = true;
    } else if (modifier === "override") {
      isOverride = true;
    } else if (modifier === "var") {
      kind = "field";
    } else if (modifier === "function") {
      kind = "method";
    } else {
      break;
    }
    // tslint:enable prefer-switch
    line = line.substring(space).trimLeft();
  }

  function indexOf(sub: string, start?: number): number | undefined {
    const idx = line.indexOf(sub, start);
    return idx < 0 ? undefined : idx;
  }

  switch (kind) {
    case "field": {
      const paren = indexOf("(");          // Haxe access modifiers: `foo(default, default)`
      const colon = indexOf(":", paren);   // Haxe type declaration
      const equals = indexOf("=", colon);  // Field value
      const semicolon = indexOf(";", equals);

      if (semicolon === undefined) {
        throw new Error("Malformed field declaration: " + line);
      }

      const name = line.substring(0, paren || colon || equals).trimRight();
      const type = (paren || colon) === undefined ? undefined :
          line.substring(paren || colon || 0, equals || semicolon).trimRight();
      const value = equals === undefined ? null :
          line.substring(equals + 1, semicolon).trimLeft();

      return {
        kind: "field", name, type, value,
        isStatic, isDynamic, isOverride, isPrivate,
      };
    }
    case "method": {
      const angle = indexOf("<");          // Haxe generics
      const paren = indexOf("(");          // Args list
      const bracket = indexOf("{", paren); // Method body
      const semicolon = indexOf(";", paren);

      if (paren === undefined || (bracket === undefined && semicolon === undefined)) {
        throw new Error("Malformed method declaration: " + line);
      }

      const nameEnd = angle !== undefined && angle < paren ? angle : paren;
      const name = line.substring(0, nameEnd).trimRight();
      const args = line.substring(nameEnd, bracket || semicolon).trimRight();

      return {
        kind: "method", name, args, body: null,
        isStatic, isDynamic, isOverride, isPrivate,
      };
    }
    default:
      return null;
  }
}

const TRANSPILE_PASSES: ((lines: Iterable<Line>) => Iterable<Line>)[] = [
  transpileContextFree,
  transpileSwitchs,
  transpileForLoops,
  removeClosureCaptureVariables,
  useTypedEntityCasts,
];

function transpileLine(line: string): string {
  // Fix variable names
  line = line.replace(/\.final\b/g, "._final");
  line = line.replace(/\.\$/g, ".__dollar__");
  line = line.replace(/\$([A-Za-z_][A-Za-z0-9_]*):/g, "__dollar__$1:");

  // Make all strings double-quoted, to have no interpolation
  line = line.replace(/"/g, "\\\"");
  line = line.replace(/\\'/g, "ø"); // tmp char
  line = line.replace(/'/g, "\"");
  line = line.replace(/ø/g, "'"); // replace tmp char

  // Fix returns
  line = line.replace(/return undefined;/g, "return;");
  // Fix int casts
  line = line.replace(/\bint\(/g, "Std.int(");
  // Fix string casts
  line = line.replace(/\b(new )?String\(/g, "'' + (");

  // Replace `prototype` method calls by `super` method calls
  // Note: this cause some issues in compiled code (which are fixed
  // later with manual patches), because Flash's `super` is weird.
  line = line.replace(/_global\.[A-Za-z0-9_.]+\.prototype\.([A-Za-z0-9_]+)\.call\(this(, )?/g, "super.$1(");

  // Replace `(foo.types & Data.TYPE) > 0` and `foo.isType(Data.TYPE)` by `Data.TYPE.check(foo)`.
  line = line.replace(/\(([^\(]+).types & ([^\)<]+)\) > 0/g, "$2.check($1)");
  line = line.replace(/\(([^\(]+).types & ([^\)<]+)\) != 0/g, "$2.check($1)");
  line = line.replace(/\(([^\(]+).types & ([^\)<]+)\) == 0/g, "!$2.check($1)");
  line = line.replace(/(.+).isType\([^\)]+\) > 0/g, "$2.check($1)");

  return line;
}

function* transpileContextFree(lines: Iterable<Line>): Iterable<Line> {
  for (const line of lines) {
    yield { pos: line.pos, indent: line.indent, line: transpileLine(line.line) };
  }
}

function* transpileSwitchs(lines: Iterable<Line>): Iterable<Line> {
  const emptyLine: Line = { pos: -1, indent: -1, line: "" };
  let linfo: Line = emptyLine;
  let switchInfo: { indent: number; caseLine: string; value: number } | undefined;
  for (const next of lines) {
    // Remove breaks in switchs
    if (next.line.endsWith(":") && (next.line.startsWith("case") || next.line.startsWith("default"))) {
      if (linfo.line.startsWith("break;")) {
        linfo = emptyLine;
      }
    }
    if (linfo === emptyLine) {
      linfo = next;
      continue;
    }

    let line = linfo.line;

    // Manage switchs with `case x++:`
    if (switchInfo !== undefined) {
      if (line === switchInfo.caseLine) {
        line = `case ${switchInfo.value++}:`;
      }
    }
    if (next.line.startsWith("switch (")) {
      const match = /var ([A-Za-z_][A-Za-z0-9_]*) = ([0-9]+);/.exec(line);
      if (match !== null) {
        switchInfo = {
          indent: next.indent,
          caseLine: `case ${match[1]}++:`,
          value: parseInt(match[2], 10),
        };
      }
    } else if (switchInfo !== undefined && switchInfo.indent === next.indent && next.line.startsWith("}")) {
      switchInfo = undefined;
    }

    yield { pos: linfo.pos, indent: linfo.indent, line };
    linfo = next;
  }

  if (linfo !== emptyLine) {
    yield linfo;
  }
}

function* transpileForLoops(lines: Iterable<Line>): Iterable<Line> {
  const deferred = [];
  for (const linfo of lines) {
    while (deferred.length > 0) { // Add lines on scope exit
      const endLine = deferred[deferred.length - 1];
      if (endLine.indent <= linfo.indent) {
        break;
      }
      yield endLine;
      deferred.pop();
    }

    let line = linfo.line;

    // Transform 'for (var i = start; i < end; i++)' into 'for (i in start...end)'
    line = line.replace(
      /for \(var ([A-Za-z_][A-Za-z0-9_]*) = (.+); \1 < (.+); \+\+\1\)/g,
      "for ($1 in $2...$3)",
    );
    // Transform infinite for loops
    line = line.replace(/for \(;;\)/g, "while (true)");

    // Transform `for (v = ...; ...; ...) {` into `while`
    // (these can't be `for`s, as `v` may be used after the loop body)
    const rawForMatch = /for \((.+); (.+); (.+)\) {/.exec(line);
    if (rawForMatch !== null) {
      yield { pos: linfo.pos, indent: linfo.indent, line: rawForMatch[1] + ";" };
      yield { pos: linfo.pos, indent: linfo.indent, line: `while (${rawForMatch[2]}) {` };
      const stmt = rawForMatch[3].replace(", ++", "; ++") + ";";
      deferred.push({ pos: linfo.pos, indent: linfo.indent + 1, line: stmt });
    } else {
      // Transform `for (<...> && !guard) {`
      // into `for (<...>) { if (guard) break;`
      const forGuardMatch = /for \((.+) && !([A-Za-z_][A-Za-z0-9_]*)\) {/.exec(line);
      if (forGuardMatch !== null) {
        yield { pos: linfo.pos, indent: linfo.indent, line: `for (${forGuardMatch[1]}) {` };
        yield { pos: linfo.pos, indent: linfo.indent + 1, line: `if (${forGuardMatch[2]}) break;` };
      } else {
        yield { pos: linfo.pos, indent: linfo.indent, line };
      }
    }
  }

  while (deferred.length > 0) {
    yield deferred.pop()!;
  }
}

function* removeClosureCaptureVariables(lines: Iterable<Line>): Iterable<Line> {
  // Variables captured by closures may spuriously take the value `undefined`, so
  // we remove them and inline their value.
  // (Haxe automatically captures `this`, so this works)
  const captureName = /\bme\b/g;
  const capturePattern = "var me = ";

  let currentCapture: string | null = null;
  let currentIndent = 0;
  for (const line of lines) {
    if (line.line.startsWith(capturePattern)) {
      if (currentCapture !== null && currentIndent !== line.indent) {
        throw new Error(`#${line.pos}: multiple capture variables in different scopes aren't supported`);
      }
      currentCapture = line.line.substring(capturePattern.length, line.line.length - 1);
      currentIndent = line.indent;
    } else if (currentCapture !== null) {
      yield { pos: line.pos, indent: line.indent, line: line.line.replace(captureName, currentCapture) };
    } else {
      yield line;
    }
  }
}

function* useTypedEntityCasts(lines: Iterable<Line>): Iterable<Line> {
  // Transform:
  // if (Data.TYPE.check(foo)) {
  //   var bar = foo;
  //
  // into:
  // var bar = Data.TYPE.downcast(foo);
  // if (bar != null) {
  let ifLine: { line: Line; name: string; type: string; extra: string | undefined } | null = null;
  for (const line of lines) {
    if (ifLine === null) {
      const ifMatch = /if \((.+).check\(([^\)]+)\)(&&.+)?\) {/
        .exec(line.line);

      if (ifMatch === null) {
        yield line;
      } else {
        ifLine = { line, type: ifMatch[1], name: ifMatch[2], extra: ifMatch[3] };
      }
      continue;
    }

    const varMatch = /var ([A-Za-z_][A-Za-z0-9_]*) = (.+);/.exec(line.line);
    if (varMatch === null || varMatch[2] !== ifLine.name) {
      yield ifLine.line;
      yield line;
      ifLine = null;
      continue;
    }

    const newName = varMatch[1];
    const asTypeLine = `var ${newName} = ${ifLine.type}.downcast(${ifLine.name});`;
    const testTypeLine = `if (${newName} != null${ifLine.extra || ""}) {`;
    yield { pos: ifLine.line.pos, indent: ifLine.line.indent, line: asTypeLine };
    yield { pos: line.pos, indent: ifLine.line.indent, line: testTypeLine };

    ifLine = null;
  }
}
