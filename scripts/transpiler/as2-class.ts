import { emitIndent, Line } from "./line-utils";

interface As2MemberBase {
  readonly name: string;
  isStatic: boolean;
  isPrivate?: boolean;
  isOverride?: boolean;
  isDynamic?: boolean;
}

export interface As2Field extends As2MemberBase {
  kind: "field";
  type?: string;
  value: string | null;
}

export interface As2Method extends As2MemberBase {
  kind: "method";
  args: string;
  body: Line[] | null;
}

export type As2Member = As2Field | As2Method;

const REGEX_IDENT = /^[A-Za-z_$][A-Za-z0-9_$]*$/;
const REGEX_IDENT_PATH = /^[A-Za-z_$][A-Za-z0-9_$]*(?:\.[A-Za-z_$][A-Za-z0-9_$]*)*/;

export class As2Class {
  public readonly name: string;
  public readonly superName: string | null;
  private _superClass?: As2Class | null;
  private _members: Map<string, As2Member>;

  constructor(name: string, superName: string | null, members?: Iterable<As2Member>) {
    if (!REGEX_IDENT_PATH.test(name)) {
      throw new Error(`'${name}' isn't a valid class name`);
    }
    if (superName !== null && !REGEX_IDENT_PATH.test(superName)) {
      throw new Error(`'${superName}' isn't a valid superclass name`);
    }
    this.name = name;
    this.superName = superName;
    this._members = new Map();
    if (members !== undefined) {
      for (const member of members) {
        this.addMember(member);
      }
    }
  }

  public static resolveSuperClasses(classes: Iterable<As2Class>): Map<string, As2Class> {
    const map: Map<string, As2Class> = new Map();
    for (const as2Class of classes) {
      if (map.has(as2Class.name)) {
        throw new Error(`Duplicate class name ${as2Class.name}`);
      }
      map.set(as2Class.name, as2Class);
    }

    for (const as2Class of map.values()) {
      if (as2Class.superName === null) {
        as2Class._superClass = null;
      } else {
        const superClass = map.get(as2Class.superName);
        if (superClass === undefined) {
          console.log(`[WARNING] Unknown super class ${as2Class.superName} for class ${as2Class.name}`);
        } else {
          as2Class._superClass = superClass;
        }
      }
    }
    return map;
  }

  public get superClass(): As2Class | null {
    if (this._superClass === undefined) {
      throw new Error(`The superclass ${this.superName} wasn't resolved for class ${this.name}`);
    }
    return this._superClass;
  }

  public get members(): ReadonlyMap<string, As2Member> {
    return this._members;
  }

  public addMember(member: As2Member): void {
    if (!REGEX_IDENT.test(member.name)) {
      throw new Error(`'${member.name}' isn't a valid member name`);
    }
    if (this._members.has(member.name)) {
      throw new Error(`Duplicate member ${member.name} in class ${this.name}`);
    }
    this._members.set(member.name, { ...member });
  }

  public removeMember(name: string): As2Member | null {
    const member = this._members.get(name);
    this._members.delete(name);
    return member || null;
  }
}

export function emitClassMember(chunks: string[], indent: number, member: As2Member): string[] {
  emitIndent(chunks, indent);
  chunks.push(member.isOverride ? "override " : "");
  chunks.push(member.isPrivate ? "private " : "public ");
  chunks.push(member.isStatic ? "static " : "");
  chunks.push(member.isDynamic ? "dynamic " : "");

  switch (member.kind) {
    case "field":
      chunks.push("var ", member.name);
      if (member.type !== undefined) {
        chunks.push(member.type);
      }
      if (member.value !== null) {
        chunks.push(" = ", member.value);
      }
      chunks.push(";\n");
      break;

    case "method":
      chunks.push("function ", member.name, member.args);
      if (member.body === null) {
        chunks.push(";\n");
      } else {
        chunks.push(" {\n");
        for (const line of member.body) {
          emitIndent(chunks, indent + line.indent);
          chunks.push(line.line, "\n");
        }
        emitIndent(chunks, indent);
        chunks.push("}\n");
      }
      break;
    default:
      throw new Error(`UnknownMemberKind: ${(member as any).kind}`);
  }
  return chunks;
}

export function emitClass(chunks: string[], as2Class: As2Class): string[] {
  const members = [...as2Class.members.values()];
  members.sort((a, b) => {
    if (a.isStatic === b.isStatic) {
      return a.kind === b.kind ? 0 : a.kind === "field" ? -1 : 1;
    }
    return a.isStatic ? -1 : 1;
  });

  chunks.push("class ", as2Class.name);
  if (as2Class.superName !== null) {
    chunks.push(" extends ", as2Class.superName);
  }
  chunks.push(" {\n");

  let prev: As2Member | undefined;
  for (const member of members) {
    const { kind, isStatic } = member;
    if (prev === undefined || prev.isStatic !== isStatic || prev.kind !== kind || kind === "method") {
      chunks.push("\n");
    }
    emitClassMember(chunks, 1, member);
    prev = member;
  }
  chunks.push("\n}\n");
  return chunks;
}
