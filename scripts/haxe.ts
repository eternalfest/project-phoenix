import { mapMovie as movieMapStrings } from "@eternalfest/obf/map-strings";
import child_process from "child_process";
import { emitSwf } from "swf-emitter";
import { parseSwf } from "swf-parser";
import { CompressionMethod, Movie, Tag, TagType } from "swf-types";
import { cleanTempFile, createFile, readFile, waitForSuccessfulExit } from "./utils";

export interface SwfHeader {
  width: number;
  height: number;
  fps: number;
  bgColor: number;
}

function getIndexOfLastTag(tags: Tag[], tagType: TagType): number | undefined {
  let i = tags.length;
  while (i > 0) {
    i--;
    if (tags[i].type === tagType) {
      return i;
    }
  }
  return undefined;
}

function mapStringsHaxe(str: string): string {
  switch (str) {
    case "downcast":
      return "cast";
    case "_final":
      return "final";
    case "_throw":
      return "throw";
    default:
      return str.replace("__dollar__", "$");
  }
}

async function injectCode(swfPath: string, srcPath: string): Promise<Movie> {
  const baseSwf = parseSwf(await readFile(swfPath));
  const codeSwf = parseSwf(await readFile(srcPath));

  const baseSwfActionTag = getIndexOfLastTag(baseSwf.tags, TagType.DoAction);
  const codeSwfActionTag = getIndexOfLastTag(codeSwf.tags, TagType.DoAction);
  if (baseSwfActionTag === undefined) {
    throw new Error("Couldn't find DoAction tag in base swf");
  } else if (codeSwfActionTag === undefined) {
    throw new Error("Couldn't find DoAction tag in code swf");
  }

  baseSwf.tags[baseSwfActionTag] = codeSwf.tags[codeSwfActionTag];
  return baseSwf;
}

async function compileSources(srcPath: string, outPath: string, header?: SwfHeader): Promise<void> {
  const haxeArgs = [
    "-main", "Main.hx",
    "-cp", srcPath,
    "-swf", outPath,
    "-swf-version", "8",
  ];

  if (header !== undefined) {
    haxeArgs.push("-swf-header", `${header.width}:${header.height}:${header.fps}:${header.bgColor}`);
  }

  const childProc = child_process.spawn("haxe", haxeArgs, { stdio: "inherit" });
  await waitForSuccessfulExit(childProc);
}

async function doCompile(swfPath: string | null, srcPath: string, outPath: string, header?: SwfHeader): Promise<void> {
  const tempHaxeOutPath = await cleanTempFile("out/tmp/src-haxe.swf");
  await compileSources(srcPath, tempHaxeOutPath, header);

  let movie: Movie;
  if (swfPath === null) {
    movie = parseSwf(await readFile(tempHaxeOutPath));
  } else {
    movie = await injectCode(swfPath, tempHaxeOutPath);
  }
  movieMapStrings(mapStringsHaxe, movie);

  await createFile(outPath, emitSwf(movie, CompressionMethod.Deflate));
}

export function compile(swfPath: string, srcPath: string, outPath: string): Promise<void> {
  return doCompile(swfPath, srcPath, outPath);
}

export function compileBare(srcPath: string, outPath: string, header: SwfHeader): Promise<void> {
  return doCompile(null, srcPath, outPath, header);
}
