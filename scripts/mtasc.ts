
import child_process from "child_process";
import path from "path";
import { emitSwf } from "swf-emitter";
import { parseSwf } from "swf-parser";
import { CompressionMethod, Movie, TagType } from "swf-types";
import { BINARY_ROOT, cleanTempFile, createFile, getExecutablePath, readFile, waitForSuccessfulExit, walkTree } from "./utils";

async function getPackageArgs(rootPath: string): Promise<string[]> {
  const args = ["-cp", rootPath, "-pack", "."];
  for await (const pack of walkTree(rootPath, "dirs")) {
    args.push("-pack", pack);
  }
  return args;
}

function removeLastDoActionTag(movie: Movie): void {
  let i = movie.tags.length;
  while (i-- > 0) {
    if (movie.tags[i].type === TagType.DoAction) {
      movie.tags.splice(i, 1);
      return;
    }
  }
}

export async function compile(swfPath: string, srcPath: string, outPath: string): Promise<void> {
  const tempMtascOutPath = await cleanTempFile("out/tmp/src-mtasc.swf");
  const mtascPath = getExecutablePath("mtasc", "mtasc");
  const mtascArgs = [
    "-swf", swfPath,
    "-out", tempMtascOutPath,
    "-cp", path.join(BINARY_ROOT, "mtasc", "std"),
    "-cp", path.join(BINARY_ROOT, "mtasc", "std8"),
    "-group", "-version", "8",
    "-main",
    ...await getPackageArgs(srcPath),
  ];

  const childProc = child_process.spawn(mtascPath, mtascArgs, {stdio: "inherit"});
  await waitForSuccessfulExit(childProc);

  const gameSwf = parseSwf(await readFile(tempMtascOutPath));
  removeLastDoActionTag(gameSwf);
  await createFile(outPath, emitSwf(gameSwf, CompressionMethod.Deflate));
}
