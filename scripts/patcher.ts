import path from "path";
import { createTextFile, fileExists, readTextFile, removeFile, walkTree } from "./utils";

/*
  Custom line-agnostic patch format for Haxe files:

  Each patch file consists of a series of patches separated by empty lines.

  Each patch starts with one of the following headers:
  - `!DEL <file>`
      Delete <file>.
  - `!ADD <len> <file>`
      Add <file>, with the contents given by the next <len> lines.
  - `!SET <len> <flie>`
      Same as !ADD, but the file already exists.
  - `!MOD <file>`
      Modify <file> with the supplied modifications.

  Modification format:
  Each modification starts with one of the following headers:
  - `!line <offsets>`: use absolute lines offsets
  - `!import <offsets>`: relative to the last import (just before the class decl. if no imports)
  - `!func <name> <offsets>`: relative to the Haxe function <name>

  Offsets are of the form `-<start>,<len> +<start>,<len>` (like the difftools format).
  Then the lines of the modification follow:
  - prefixed by ` `: the line wasn't changed
  - prefixed by `+`: the line was added
  - prefixed by `-`: the line was removed
 */

interface Line {
  idx: number;
  content: string;
}

interface Range {
  start: number;
  length: number;
}

interface SourceInfos {
  fileName: string;
  imports: number | null;
  funcs: Map<string, number>;
}

interface ModifyPatch {
  headerIdx: number;
  patchStart: number;
  lines: Line[];
}

interface PatchHeader {
  kind: string;
  filePath: string;
  length: number | null;
}

function parseUint(str: string): number | null {
  const num: number = parseInt(str, 10);
  return !isFinite(num) || num < 0 ? null : num;
}

function next<T>(iterator: Iterator<T, any, undefined>): T | null {
  const next = iterator.next();
  return next.done ? null : next.value;
}

function parseModificationBounds(str: string, prefix: string): Range | null {
  if (!str.startsWith(prefix)) {
    return null;
  }
  const parts = str.substr(prefix.length).split(",");
  if (parts.length !== 2) {
    return null;
  }
  const start = parseUint(parts[0]);
  const length = parseUint(parts[1]);
  if (start === null || length === null) {
    return null;
  }
  return { start, length };
}

function parseOneModifyPatch(patchName: string, patchLines: Iterator<Line>, file: SourceInfos): ModifyPatch | null {
  const header = next(patchLines);
  if (header === null || header.content === "") {
    return null; // end of modify patches
  }

  // Parse header kind and derive offset
  const splitted = header.content.split(" ");
  const kind = splitted.shift();
  let offset: number;
  switch (kind) {
    case "!line":
      offset = 0;
      break;
    case "!import":
      if (file.imports === null) {
        throw new Error(`Patch ${patchName}:${header.idx}: no imports in file ${file.fileName}`);
      }
      offset = file.imports;
      break;
    case "!func":
      const funcName = splitted.shift() || "";
      const off = file.funcs.get(funcName);
      if (off === undefined) {
        throw new Error(`Patch ${patchName}:${header.idx}: couldn't find function '${funcName}' in file ${file.fileName}`);
      }
      offset = off;
      break;
    default:
      throw new Error(`Patch ${patchName}:${header.idx}: unknown modification kind '${kind}'`);
  }

  const rem = parseModificationBounds(splitted.shift() || "", "-");
  const add = parseModificationBounds(splitted.shift() || "", "+");
  if (splitted.length > 0 || rem === null || add === null || rem.start !== add.start) {
    throw new Error(`Patch ${patchName}:${header.idx}: invalid header '${header.content}'`);
  }

  // Read lines
  const lines: Line[] = [];
  while (true) {
    const patchLine = next(patchLines);
    if (patchLine === null) {
      throw new Error(`Patch ${patchName}:${header.idx}: not enough lines`);
    }

    lines.push(patchLine);
    switch (patchLine.content.charAt(0)) {
      case "+":
        add.length--;
        break;
      case "-":
        rem.length--;
        break;
      case " ":
        add.length--;
        rem.length--;
        break;
      default:
        throw new Error(`Patch ${patchName}:${patchLine.idx}: invalid line`);
    }

    if (add.length < 0 || rem.length < 0) {
      throw new Error(`Patch ${patchName}:${patchLine.idx}: too many lines`);
    } else if (add.length === 0 && rem.length === 0) {
      return {
        headerIdx: header.idx,
        patchStart: offset + rem.start,
        lines,
      };
    }
  }
}

const REGEX_HAXE_FUNCTION = /^\s*(?:(?:public|private|static|override|dynamic)\s+)+function\s+([A-Za-z0-9_]+)[(<].+\{/;
function gatherSourceInfos(fileName: string, lines: string[]): SourceInfos {
  const funcs: Map<string, number> = new Map();
  let imports: number | null = null;
  let i = 0;
  for (const line of lines) {
    i++;

    if (imports === null) {
      if (line === "") {
        imports = i;
        continue;
      }
    } else if (line.startsWith("import")) {
      imports = i;
      continue;
    }

    const match = REGEX_HAXE_FUNCTION.exec(line);
    if (match !== null) {
      const funcName: string = match[1];
      if (funcs.has(funcName)) {
        console.log(`WARNING: duplicate function decl at l.${funcs.get(funcName)} and l.${i} in ${fileName}`);
      }
      funcs.set(funcName, i);
    }
  }

  return { fileName, funcs, imports };
}

function processModifyPatches(patchName: string, patchLines: Iterator<Line>,
    fileLines: string[], fileInfos: SourceInfos): string[] {
  // Parse all patches and sort them by line number
  const patches: ModifyPatch[] = [];
  while (true) {
    const p = parseOneModifyPatch(patchName, patchLines, fileInfos);
    if (p === null) {
      break;
    }
    patches.push(p);
  }
  patches.sort((a, b) => a.patchStart - b.patchStart);

  const lines: string[] = [];
  let fileIdx = 1;
  // Reverse file lines so we can pop() them
  fileLines.reverse();
  for (const patch of patches) {
    if (fileIdx > patch.patchStart) {
      throw new Error(`Patch ${patchName}:${patch.headerIdx}: overlapping patches`);
    }

    // Output lines until we get to the first line to replace
    while (fileIdx < patch.patchStart) {
      lines.push(fileLines.pop()!);
      fileIdx++;
    }

    // Apply changes
    for (const patchLine of patch.lines) {
      const kind = patchLine.content.charAt(0);
      const line = patchLine.content.substr(1);
      if (kind === "+") {
        lines.push(line);
      } else {
        const fileLine = fileLines.pop();
        if (fileLine !== line) {
          throw new Error(`Patch ${patchName}:${patchLine.idx}: line doesn't match source at ${fileInfos.fileName}:${fileIdx}`);
        } else if (kind === " ") {
          lines.push(line);
        }
        fileIdx++;
      }
    }
  }

  // Write the last lines
  while (fileLines.length > 0) {
    lines.push(fileLines.pop()!);
  }
  return lines;
}

function* iterateLines(text: string): Iterable<Line> {
  let idx = 1;
  for (const line of text.split("\n")) {
    const result = { idx, content: line };
    yield result;
    idx++;
  }
}

function getNextMatching<T>(iterator: Iterator<T>, filter: (item: T) => boolean): T | null {
  while (true) {
    const item = iterator.next();
    if (item.done) {
      return null;
    } else if (filter(item.value)) {
      return item.value;
    }
  }
}

function parsePatchHeader(header: string): PatchHeader | null {
  const splitted = header.split(" ");
  const kind = splitted.shift();
  const filePath = splitted.pop();
  let length: number | null = null;
  if (kind === undefined || filePath === undefined) {
    return null;
  }
  if (kind === "!ADD" || kind === "!SET") {
    length = parseUint(splitted.pop() || "");
    if (length === null) {
      return null;
    }
  }

  if (splitted.length > 0) {
    return null;
  }
  return {
    kind, length,
    filePath: path.join(...filePath.split("/")),
   };
}

export async function applyPatch(patchFile: string, targetDir: string): Promise<void> {
  const patchContents = await readTextFile(patchFile);
  if (!patchContents.endsWith("\n")) {
    throw new Error(`Patch ${patchFile}: file ended without a newline`);
  }

  const patchLines: Iterator<Line> = iterateLines(patchContents)[Symbol.iterator]();
  const pathsPatched: Set<string> = new Set();
  const actions: (() => Promise<void>)[] = [];

  while (true) {
    const headerLine = getNextMatching(patchLines, h => h.content.length > 0);
    if (headerLine === null) {
      break; // end of file
    }

    const header = parsePatchHeader(headerLine.content);
    if (header === null) {
      throw new Error(`Patch ${patchFile}:${headerLine.idx}: invalid header '${headerLine.content}`);
    }
    const targetPath = path.normalize(path.join(targetDir, header.filePath));
    if (pathsPatched.has(targetPath)) {
      throw new Error(`Patch ${patchFile}:${headerLine.idx}: duplicate patched file ${targetPath}`);
    }
    pathsPatched.add(targetPath);

    const pathExists = await fileExists(targetPath);
    let checkEmptyLine: boolean = true;
    switch (header.kind) {
      case "!DEL":
        if (!pathExists) {
          throw new Error(`Patch ${patchFile}:${headerLine.idx}: can't remove non-existing file ${targetPath}`);
        }
        actions.push(() => removeFile(targetPath));
        break;
      case "!ADD":
      case "!SET":
        if (header.kind === "!ADD" && pathExists) {
          throw new Error(`Patch ${patchFile}:${headerLine.idx}: can't create existing file ${targetPath}`);
        } else if (header.kind === "!SET" && !pathExists) {
          throw new Error(`Patch ${patchFile}:${headerLine.idx}: can't replace non-existing file ${targetPath}`);
        }

        const lines: string[] = [];
        while (lines.length !== header.length) {
          const line = next(patchLines);
          if (line === null) {
            throw new Error(`Patch ${patchFile}:${headerLine.idx}: unexpected end of file`);
          }
          lines.push(line.content);
        }
        actions.push(() => createTextFile(targetPath, lines.join("\n")));
        break;
      case "!MOD":
        if (!pathExists) {
          throw new Error(`Patch ${patchFile}:${headerLine.idx}: can't modify non-existing file ${targetPath}`);
        }

        const fileLines = (await readTextFile(targetPath)).split("\n");
        const sourceInfos = gatherSourceInfos(targetPath, fileLines);
        const modifiedLines = processModifyPatches(patchFile, patchLines, fileLines, sourceInfos);
        actions.push(() => createTextFile(targetPath, modifiedLines.join("\n")));
        checkEmptyLine = false;
        break;
      default:
        throw new Error(`Patch ${patchFile}:${headerLine.idx}: unknown patch kind '${header.kind}'`);
    }

    if (checkEmptyLine) {
      const line = next(patchLines);
      if (line !== null && line.content.length > 0) {
        throw new Error(`Patch ${patchFile}:${line.idx}: expected empty line after patch`);
      }
    }
  }

  if (actions.length === 0) {
    throw new Error(`Patch ${patchFile} is empty`);
  }

  await Promise.all(actions.map(f => f()));
}

export async function applyPatchesInDir(patchesDir: string, targetDir: string): Promise<void> {
  for await(const file of walkTree(patchesDir, "files")) {
    const patchFile = path.join(patchesDir, file);
    await applyPatch(patchFile, targetDir);
  }
}
