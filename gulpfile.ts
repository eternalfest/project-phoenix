import gulp from "gulp";
import tslint from "gulp-tslint";
import path from "path";

function generateTslintTask(sourceDir: string, format: boolean): gulp.TaskFunction {
  return async function () {
    gulp.src(["gulpfile.ts", path.join(sourceDir, "**/*.ts"), "!" + path.join(sourceDir, "__pycache__/**")])
    .pipe(tslint({
      formatter: "msbuild",
      rulesDirectory: ".",
      fix: format,
    }))
    .pipe(tslint.report({
      summarizeFailureOutput: true,
    }));
  };
}

gulp.task("lint", generateTslintTask("./scripts", false));
gulp.task("format", generateTslintTask("./scripts", true));

gulp.task("decompile:copy-game", async function () {
  const utils = await import("./scripts/utils");
  await utils.copyFile("game/src/lib/game.swf", "out/game.swf");
});

gulp.task("decompile:deobfuscate", async function () {
  const deobfuscator = await import("./scripts/deobfuscator");
  await deobfuscator.deobfuscate("out/game.swf", "out/game.clear.swf");
});

gulp.task("decompile:decompile", async function () {
  const flare = await import("./scripts/flare");
  await flare.decompile("out/game.clear.swf", "out/game.clear.flr");
});

gulp.task("decompile:transpile", async function () {
  const transpiler = await import("./scripts/transpiler");
  await transpiler.transpile("out/game.clear.flr", "out/src-mtasc", "out/globals", "out/src-haxe");
});

gulp.task("decompile:patch:mtasc", async function () {
  const patcher = await import("./scripts/patcher");
  await patcher.applyPatchesInDir("patchs/mtasc", "out/src-mtasc");
});

gulp.task("decompile:patch:haxe", async function () {
  const patcher = await import("./scripts/patcher");
  await patcher.applyPatchesInDir("patchs/haxe", "out/src-haxe");
});

gulp.task("decompile:patch", gulp.parallel("decompile:patch:mtasc", "decompile:patch:haxe"));
gulp.task("decompile", gulp.series(
  "decompile:copy-game",
  "decompile:deobfuscate", "decompile:decompile",
  "decompile:transpile", "decompile:patch",
));

gulp.task("recompile:mtasc:compile", async function () {
  const mtasc = await import("./scripts/mtasc");
  await mtasc.compile("out/game.clear.swf", "out/src-mtasc", "out/game-mtasc.clear.swf");
});

gulp.task("recompile:mtasc:obfuscate", async function () {
  const obfuscator = await import("./scripts/obfuscator");
  await obfuscator.obfuscate("out/game-mtasc.clear.swf", "out/game-mtasc.swf");
});

gulp.task("recompile:haxe:compile", async function () {
  const haxe = await import("./scripts/haxe");
  await haxe.compile("out/game.clear.swf", "out/src-haxe", "out/game-haxe.clear.swf");
});

gulp.task("recompile:haxe:obfuscate", async function () {
  const obfuscator = await import("./scripts/obfuscator");
  await obfuscator.obfuscate("out/game-haxe.clear.swf", "out/game-haxe.swf");
});

gulp.task("recompile:mtasc", gulp.series("recompile:mtasc:compile", "recompile:mtasc:obfuscate"));
gulp.task("recompile:haxe", gulp.series("recompile:haxe:compile", "recompile:haxe:obfuscate"));
gulp.task("recompile", gulp.parallel("recompile:mtasc", "recompile:haxe"));

gulp.task("microloader:compile", async function () {
  const haxe = await import("./scripts/haxe");
  await haxe.compileBare("microloader/src", "out/microloader.clear.swf", {
    width: 420,
    height: 520,
    fps: 40,
    bgColor: 0x000000,
  });
});

gulp.task("microloader:obfuscate", async function () {
  const obfuscator = await import("./scripts/obfuscator");
  await obfuscator.obfuscate("out/microloader.clear.swf", "out/microloader.swf");
});

gulp.task("microloader", gulp.series("microloader:compile", "microloader:obfuscate"));
