# Loader API

This document describes the environment and APIs that must be provided by the loader for the game to work correctly.

All identifiers prefixed by `*` should be obfuscated.

# Injected methods and classes

The following classes must be provided in the game's `MovieClip`: `*Std`, `*Hash`, `*Log` (see full declarations in `game-types` repository).

The following methods must be injected into existing AS2 classes:
|   Class   |    Method    |                  Signature                  |
| :-------: | :----------: | :-----------------------------------------: |
| `String`  |   `*slice`   |      `(start: Int, end: Int): String`       |
|  `Array`  | `*duplicate` |                 `(): Array`                 |
|  `Color`  |   `*reset`   |                 `(): Void`                  |
| `XMLNode` |    `*get`    |          `(attr: String): String`           |
| `XMLNode` |    `*set`    | `(attr: String, value: Null<String>): Void` |

# Injected global variables

The following global variables should be injected into the root `MovieClip`:

|    Name     |                            Values                            |
| :---------: | :----------------------------------------------------------: |
|  `$volume`  |                    `"0"` through `"100"`                     |
|  `$sound`   |                        `"0"` or `"1"`                        |
|  `$music`   |                        `"0"` or `"1"`                        |
|  `$detail`  |                        `"0"` or `"1"`                        |
|  `$shake`   |                        `"0"` or `"1"`                        |
|   `$mode`   |                   Mode name, e.g. `"solo"`                   |
| `$version`  |                 Flash Player version string                  |
|   `$out`    |              URL to redirect to after game end               |
| `*gameOver` | Called when ending a Adventure game<br />`(score1: Int, score2: Int, data: Dynamic): Void` |
| `*exitGame` | Called to change the redirect URL when exiting the game<br />`(url: String, params: Dynamic): Void` |



# Game interaction

**TODO:** Is it required to load the game into a separate `MovieClip` or can it be loaded at the root of the movie?

1. Call the constructor of the `*GameManager` class to create an instance of the game.

2. Call the `*GameManager.main()` method on each frame.

The constructor has the following signature: `GameManager(mc: MovieClip, options: Options)`, where:

- `mc` is a reference to the game's `MovieClip`.
- `options` is an object with the following members:
  - `*fl_local: Bool`: is the game played locally?
  - `*rawLang: String`: the `lang.xml` to use for texts;
  - `*musics: Array<Sound>`: the musics for the game (empty if music is disabled);
  - `*families: String`: comma-separated list of families available;
  - `*options: String`: commad-separated list of options enabled.