# Ruffle issues

Tested on Ruffle commit `e25d9719`.

## Bugs

- Messages dans la console:
    - `WARN symphonia_bundle_mp3::layer3: mpa: invalid main_data_begin, underflow by 65 bytes`
    - Cause et conséquence inconnue

## Cosmetic bugs

- Quality seems stuck to 'low'. (related to [#9531](https://github.com/ruffle-rs/ruffle/issues/9531)?)
- BottomBar cristal letter animations don't look right (cause unknown)
- Slightly worse text anti-aliasing, default font is different?
- Igor and torch halos are too faint (best visible without the darkness-reducing quests) ([#14959](https://github.com/ruffle-rs/ruffle/issues/14959))
- ...

## Worked around

...
