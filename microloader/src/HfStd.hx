import flash.MovieClip;
import flash.TextField;

class HfStd {

  
  public static function __init(): Void untyped {
    // Unprotect `Array` prototype (Haxe protects it).
    _global.ASSetPropFlags(Array.prototype, null, 0, 7);

    // Array.remove is already added by Haxe.
    // Array.insert is already added by Haxe.
    Array.prototype.duplicate = Array.prototype.copy;

    Color.prototype.reset = function() {
      __this__.setTransform({ ra : 100, rb : 0, ba : 100, bb : 0, ga : 100, gb : 0, aa : 100, ab : 0 });
    }
    XMLNode.prototype.get = function(x) {
      return __this__.attributes[x];
    }
    XMLNode.prototype.set = function(x,y) {
      __this__.attributes[x] = y;
    }
    XMLNode.prototype.exists = function(x) {
      return __this__.attributes[x] != null;
    }

    XMLNode.prototype.attributesIter = function(f) {
      for (attributeName in Reflect.fields(__this__.attributes)) {
        f(attributeName, __this__.attributes[attributeName]);
      }
    }
    XMLNode.prototype.childrenIter = function(f) {
      var x = __this__.firstChild;
      while( x != null ) {
        f(x);
        x = x.nextSibling;
      }
    }
    
    XML.prototype.get = function(x) {
      return __this__.attributes[x];
    }
    XML.prototype.set = function(x,y) {
      __this__.attributes[x] = y;
    }
    XML.prototype.exists = function(x) {
      return __this__.attributes[x] != null;
    }

    _global.ASSetPropFlags(Array.prototype, "duplicate", 1);
    _global.ASSetPropFlags(Color.prototype, "reset", 1);
    _global.ASSetPropFlags(XMLNode.prototype, "get", 1);
    _global.ASSetPropFlags(XMLNode.prototype, "set", 1);
    _global.ASSetPropFlags(XMLNode.prototype, "exists", 1);
    _global.ASSetPropFlags(XMLNode.prototype, "attributesIter", 1);
    _global.ASSetPropFlags(XMLNode.prototype, "childrenIter", 1);
    _global.ASSetPropFlags(XML.prototype, "get", 1);
    _global.ASSetPropFlags(XML.prototype, "set", 1);
    _global.ASSetPropFlags(XML.prototype, "exists", 1);
  }

  public static var icounter = 0;

  public static function attachMC(mc: MovieClip, link: String, depth: Int): MovieClip {
    var inst = link + "@" + HfStd.icounter++;
    return mc.attachMovie(link, inst, depth);
  }

  public static function createEmptyMC(mc: MovieClip, depth: Int): MovieClip {
    var inst = "empty@" + HfStd.icounter++;
    return mc.createEmptyMovieClip(inst, depth);
  }

  public static function duplicateMC(mc: MovieClip, depth: Int): MovieClip {
    var inst = "dup@" + HfStd.icounter++;
    return mc.duplicateMovieClip(inst, depth);
  }

  public static function getVar(mc: MovieClip, v: String): Dynamic {
    return Reflect.field(mc, v);
  }

  public static function setVar(mc: MovieClip, v: String, vval: Dynamic): Void {
    Reflect.setField(mc, v, vval);
  }

  public static function getRoot(): Dynamic {
    return untyped _root;
  }

  public static function getGlobal(v: String): Dynamic {
    return untyped _global[v];
  }

  public static function setGlobal(v: String, vval: Dynamic): Void {
    untyped _global[v] = vval;
  }

  public static function createTextField(mc: MovieClip, depth: Int): TextField {
    var inst = "text@" + HfStd.icounter++;
    return mc.createTextField(inst, depth, 0, 0, 100, 20);
  }

  // Will become `cast` once compiled.
  public static function _cast(x: Dynamic): Dynamic {
    return x;
  }

  public static function hitTest(mc1: MovieClip, mc2: MovieClip) {
    return mc1.hitTest(mc2);
  }

  public static function random(n: Int): Int {
    return untyped __random__(n);
  }
  
  public static function xmouse(): Float {
    return flash.Lib._root.xmouse;
  }

  public static function ymouse(): Float {
    return flash.Lib._root.ymouse;
  }

  public static function escape(s: String): String {
    return untyped _global.escape(s);
  }

  public static function unescape(s: String): String {
    return untyped _global.unescape(s);
  }

  public static function parseInt(s: String): Int {
    return untyped _global.parseInt(s);
  }

  public static function toString(x: Dynamic): String {
    return untyped String(x);
  }

  public static function toStringBase(x: Int, n: Int): String {
    return untyped Number(x).toString(n);
  }

  public static function isNaN(x: Float): Bool {
    return untyped _global.isNaN(x);
  }

  public static var infinity(default, never): Float = untyped _global.Infinity;

  public static function registerClass(link: String, cls: Dynamic): Void {
    return untyped Object.registerClass(link, cls);
  }

  public static function copy(a: Dynamic, b: Dynamic): Void {
    for (key in Reflect.fields(b)) {
      Reflect.setField(a, key, Reflect.field(b, key));
    }
  }

  public static function callback(o: Dynamic, f: String): Dynamic {
    var args: Array<Dynamic> = untyped __arguments__;
    if (args.length == 2) {
      return function() {
        return untyped o[f].apply(o, __arguments__);
      };
    } else {
      args = args.slice(2);
      return function() {
        return untyped o[f].apply(o, a.concat(__arguments__));
      };
    }
  }

  public static function getTimer(): Int {
    return untyped __gettimer__();
  }

  public static function _typeof(o: Dynamic): String {
    return untyped __typeof__(o);
  }

  public static function _instanceof(o: Dynamic, cls: Dynamic): Bool {
    return untyped __instanceof__(o, cls);
  }

  public static function forin(o: Dynamic, f: String -> Void): Void {
    for(k in Reflect.fields(o))
      f(k);
  }

  public static function makeNew(o: Dynamic, ?a1: Dynamic, ?a2: Dynamic, ?a3: Dynamic, ?a4: Dynamic): Dynamic {
    return untyped __new__(o, a1, a2, a3, a4);
  }

  public static function deleteField(o: Dynamic, k: String): Void {
    Reflect.deleteField(o, k);
  }

  public static function fscommand(c: String, a: String): Void {
    flash.Lib.fscommand(c, a);
  }

  // Will become `throw` once compiled.
  public static function _throw(o: Dynamic): Dynamic {
    throw o;
  }
}
