
class Hash {

  private var h: Dynamic;

  public function new() {
    this.h = {};
  }

  public function get(k: String): Dynamic {
    return Reflect.field(this.h, k);
  }

  public function set(k: String, v: Dynamic): Void {
    Reflect.setField(this.h, k, v);
  }

  public function remove(k: String): Bool {
    var present = Reflect.field(this.h, k) != null;
    Reflect.deleteField(this.h, k);
    return present;
  }

  public function exists(k: String): Bool {
    return Reflect.field(this.h, k) != null;
  }

  public function iter(fn: String -> Dynamic -> Void): Void {
    for (k in Reflect.fields(this.h)) {
      fn(k, Reflect.field(this.h, k));
    }
  }
}
