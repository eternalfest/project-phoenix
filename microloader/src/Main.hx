import flash.MovieClip;
import flash.MovieClipLoader;

class Main {

  public static var LOADER_VERSION(default, never): String = "v0.1";
  public static var GAME_NAME(default, never): String = Macro.obfuRaw("game");
  public static var GAME_EXT(default, never): String = Macro.obfuRaw("swf");
  public static var IS_OBFU(default, never): Bool = "x" != Macro.obfuRaw("x");
  
  public static var LANG_FR_XML(default, never): String = Macro.includeFile("../lang.fr.xml");
  public static var DEFAULT_LOCAL(default, never): Bool = false;
  public static var DEFAULT_MODE(default, never): String = Macro.obfuRaw("solo");
  public static var DEFAULT_OPTIONS(default, never): String = Macro.obfuRaw("boost");
  public static var DEFAULT_FAMILIES(default, never): String = "0,1,2,3,4,10,11,12,19,100,101,102,103,104,105,106,107,108,109,110,"
    + "111,112,113,1000,1001,1002,1003,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1016,1017,1018,1023,1025,1026,"
    + "1030,5000,5001,5002,5003,5004,5005,5006,5007,5008,5009,5010,5012,5014";

  static function main() {
    Log.setColor(0xFFFF00);

    var gameUrl = getGameUrl();
    Log.trace('Microloader $LOADER_VERSION (obfuscated: $IS_OBFU)');
    Log.trace("Loading game... (" + gameUrl + ")");

    var gameMC = flash.Lib._root.createEmptyMovieClip("main@game", 1);

    var gameMCLoader = new MovieClipLoader();
    gameMCLoader.onLoadError = Main.onGameLoadError;
    gameMCLoader.onLoadComplete = Main.onGameLoaded;
    gameMCLoader.onLoadInit = Main.onGameReady;
    gameMCLoader.loadClip(gameUrl, gameMC);
  }

  private static function onGameLoaded(gameMC: MovieClip): Void {
    // Inject the classes required by the game.
    HfStd.__init();
    Reflect.setField(gameMC, "Log", Log);
    Reflect.setField(gameMC, "Hash", Hash);
    Reflect.setField(gameMC, "Std", HfStd);
  }
  
  private static function onGameReady(gameMC: MovieClip): Void {
    Log.trace("Game loaded!");

    injectGameVar("$volume", "0");
    injectGameVar("$sound", "0");
    injectGameVar("$music", "0");
    injectGameVar("$detail", "0");
    injectGameVar("$shake", "0");
    injectGameVar("$mode", DEFAULT_MODE);

    Log.trace("local = " + DEFAULT_LOCAL);
    Log.trace("mode = " + DEFAULT_MODE);
    Log.trace("options = " + DEFAULT_OPTIONS);
    Log.trace("families = " + DEFAULT_FAMILIES);

    prepareClickableArea(flash.Lib._root, gameMC);
    Log.trace("Click to start...");
  }

  private static function onGameLoadError(gameMC: MovieClip, msg: String): Void {
    Log.trace("Game loading failed: " + msg);
  }

  private static function onGameStart(gameMC: MovieClip): Void {
    Log.clear();

    var gMan: Dynamic = untyped __new__(gameMC.GameManager, gameMC, {
      rawLang: LANG_FR_XML,
      fl_local: DEFAULT_LOCAL,
      families: DEFAULT_FAMILIES,
      options: DEFAULT_OPTIONS,
      musics: [], // We don't support musics.
    });

    flash.Lib.current.onEnterFrame = function() {
      gMan.main();
    };
  }

  private static function prepareClickableArea(mc: MovieClip, game: MovieClip): Void {
    // Make an invisible clickable area.
    var click = mc.createEmptyMovieClip("main@click", 2);

    click.beginFill(0x000000, 0);
    var w = flash.Stage.width;
    var h = flash.Stage.height;
    click.moveTo(0, 0);
    click.lineTo(0, h);
    click.lineTo(w, h);
    click.lineTo(w, 0);
    click.endFill();

    click.onRelease = function() {
      click.onRelease = null;
      click.removeMovieClip();
      Main.onGameStart(game);
    };
  }

  private static function injectGameVar(name: String, value: String): Void {
    Reflect.setField(flash.Lib._root, name, value);
    Log.trace(name + " = " + value);
  }

  private static function getGameUrl(): String {
    return IS_OBFU ? '$GAME_NAME.$GAME_EXT' : '$GAME_NAME.clear.$GAME_EXT';
  }
}
