
import flash.MovieClip;
import flash.TextField;
import flash.Stage;

class Log {
  public static var std: Dynamic = HfStd;
  public static var initdone: Bool = false;
  public static var maxHeight: Null<Float>;
  public static var ttrace: Null<TextField>;
  public static var tprint: Null<TextField>;
  public static var tid: Null<Int>;


  public static function clear(): Void {
    Log.tprint.text = "";
    Log.ttrace.text = "";
  }

  public static function init(?mc: MovieClip, ?w: Float, ?h: Float): Void {
    if (mc == null) mc = flash.Lib._root;
    if (w == null) w = Stage.width;
    if (h == null) h = Stage.height;
    Log.maxHeight = h - 30;
    Log.initdone = true;
    
    Log.ttrace = mc.createTextField("text@log-trace", 99999, 5, 20, w-10, h);
    Log.tprint = mc.createTextField("text@log-print", 99998, w/2, 20, w/2, h-30);

    Log.ttrace.wordWrap = true;
    Log.tprint.wordWrap = true;
    Log.ttrace.selectable = false;
    Log.tprint.selectable = false;
    Log.tid = (untyped _global["setInterval"])(function() {
      if(Log.tprint.text != "")
        Log.tprint.text = "";
    }, 10);
  }

  public static function traceString(s: String): Void {
    if (!Log.initdone) Log.init();
    Log.ttrace.text += s.split("\n").join("\\n").split("\r").join("\\r")+"\n";
    while (Log.ttrace.textHeight > Log.maxHeight) {
      var p = Log.ttrace.text.indexOf("\r",0);
      var s2 = Log.ttrace.text.substr(p+1);
      Log.ttrace.text = s2;
    }
  }

  public static function setColor(c: Int): Void {
    if (!Log.initdone) Log.init();
    Log.ttrace.textColor = c;
    Log.tprint.textColor = c;
  }

  public static function print(x: Dynamic): Void {
    if (!Log.initdone) Log.init();
    Log.tprint.text += "~ "+ Log.toString(x,"") + "\n";
  }

  public static function destroy(): Void {
    (untyped _global["clearInterval"])(Log.tid);
    Log.initdone = false;
    Log.ttrace.removeTextField();
    Log.tprint.removeTextField();
  }

  public static function toString(o: Dynamic, ?s: String): String {
    if (s == null) {
      s = '';
    } else if (s.length >= 20) {
      return '<...>';
    }
    var argType: String = untyped __typeof__(o);
    if (argType == Macro.obfuRaw("object") || argType == Macro.obfuRaw("movieclip")) {
      var txt;
      if (untyped __instanceof__(o, Array)) {
        var c = o.length;
        var i = 0;
        txt = '[';
        s += '    ';
        while (i < c) {
          txt += (i > 0 ? ',' : '') + Log.toString(o[i], s);
          i++;
        }
        s = s.substring(4);
        txt += ']';
      } else {
        var str = o.toString();
        if (untyped __typeof__(str) == 'string' && str != '[object Object]') {
          return str;
        }
        var key;
        txt = '{\n';
        if (untyped __typeof__(o) == 'movieclip') {
          txt = 'MC(' + o._name + ') ' + txt;
        }
        s += '    ';
        for (key in Reflect.fields(o)) {
          txt += s + key + ' : ' + Log.toString(Reflect.field(o, key), s) + '\n';
        }
        s = s.substring(4);
        txt += s + '}';
      }
      return txt;
    } else if (argType == Macro.obfuRaw("function")) {
      return '<fun>';
    } else if (argType == Macro.obfuRaw("string")) {
      return o;
    } else {
      return "" + o;
    }
  }

  public static function trace(o: Dynamic): Void {
    var s = Log.toString(o,"").split("\n");
    var i = 0;
    var l = s.length;
    while (i < l)
      Log.traceString(s[i++]);
  }
}
