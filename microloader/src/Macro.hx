
import haxe.macro.Expr;

#if macro
import haxe.macro.Context;
import haxe.io.Path;
import sys.io.File;
#end

class Macro {
  public static macro function includeFile(path: String): ExprOf<String> {
    var posInfos = Context.getPosInfos(Context.currentPos());
    var path = Path.join([Path.directory(posInfos.file), path]);

    var contents = File.getContent(path);
    return macro $v{contents};
  }

  public static macro function obfuRaw(ident: String): ExprOf<String> {
    return macro $v{"$" + ident}.substring(1);
  }
}
