# Project Phoenix

Provide scripts to generate readable (and compilable!) code from the Hammerfest game SWF.

Download with `git clone https://gitlab.com/eternalfest/project-phoenix.git --recurse-submodules`.

## Dependencies

The following dependencies are needed:
  - `node` and `yarn`, to execute the scripts (don't forget to `yarn install`!)
  - `haxe` (v3.1.3), for compiling Haxe sources

The `mtasc` and `flare` programs are also needed, but they are bundled in `bin/` for convenience.

Make sure that the `game` and `game-types` submodules are up-to-date by running `update-submodules.sh`.

## Decompiling the game

Run `yarn decompile` to decompile the game. This will produce the following outputs:
  - `out/game.clear.swf`: the deobfuscated game
  - `out/game.clear.flr`: the raw decompiled code
  - `out/globals`: extracted global variables definitions
  - `out/src-mtasc/`: transpiled AS2 sources, compilable with MTASC
  - `out/src-haxe/`: transpiled Haxe source

## Recompiling the game

Run `yarn recompile` to recompile the game (you need to decompile it first!). This will produce the following outputs:
  - `out/game-mtasc.clear.swf`: the recompiled game from MTASC sources, not obfuscated
  - `out/game-mtasc.swf`: the recompiled game from MTASC sources, obfuscated
  - `out/game-haxe.clear.swf`: the recompiled game from Haxe sources, not obfuscated
  - `out/game-haxe.swf`: the recompiled game from Haxe sources, obfuscated

You can test the recompiled game by using the `eternalfest` projects in `tests/`.

## Microloader

A minimal standalone loader is provided for testing purposes.  
Run `yarn microloader` to compile it. This will produce the following outputs:
  - `out/microloader.clear.swf`: the unobfuscated microloader, for use with the non-obfuscated game
  - `out/microloader.swf`: the obfuscated microloader, for use with the obfuscated game

## Known bugs

### MTASC game
  - The fish items' cristals don't spawn at the end of the level, because of a weird issue with variables captured in functions.

### Haxe game
  - None.

### Microloader
  - `microloader.clear.swf` doesn't launch properly.
